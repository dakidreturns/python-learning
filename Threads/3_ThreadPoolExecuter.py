import concurrent.futures
import logging
import time
from concurrent.futures import as_completed
import random


def thread_function(name, timeOfSleep):
    logging.info(f"Thread {name}: starting")
    time.sleep(timeOfSleep)
    logging.info(f"Thread {name}: finishing")
    return {"threadName": name, "someValue": random.randint(0,100)}


if __name__ == "__main__":
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO, datefmt="%H:%M:%S")

    with concurrent.futures.ThreadPoolExecutor(max_workers=3) as executor:
        futures = [executor.submit(thread_function, i,2 ) for i in range(3)]
    
    for future in as_completed(futures):
        result = future.result()
        logging.info(f"Thread {result["threadName"]}: Returned Value: {result['someValue']}") 