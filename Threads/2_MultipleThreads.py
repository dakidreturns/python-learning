import logging
import threading
import time

def thread_function(name, sleepTime):
    logging.info(f"Thread {name}: starting")
    time.sleep(sleepTime)
    logging.info(f"Thread {name}: finishing")

if __name__=="__main__":
    format  = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,datefmt="%H:%M:%S")

    threads = list()
    for index in range(3):
        logging.info("Main    : create and start thread %d.", index)
        x = threading.Thread(target=thread_function, args=(index,2))
        threads.append(x)
        x.start()
    
    for index, thread in enumerate(threads):
        logging.info("Main    : before joining thread %d.", index)
        thread.join()
        logging.info("Main    : thread %d done", index)