import multiprocessing
import constants
import time
import logging
import threading 

def longCalculation(end):
    i = 0
    while i<end:
        i = i+1


def init():
    logging.basicConfig(format=constants.LOGGINGFORMAT,datefmt=constants.DATETIMEFORMAT,level=logging.INFO)

if __name__ == "__main__":
    init()

    # client = multiprocessing.Process()
    count = 100000000
    
    start = time.time()
    longCalculation(count)
    end = time.time() - start

    logging.info(f"Time Taken for single thread : {end}")

    start = time.time()
    x = threading.Thread(target=longCalculation, args=(count//2,))
    y = threading.Thread(target=longCalculation, args=(count//2,))
    x.start()
    y.start()
    x.join()
    y.join()
    end = time.time() - start
    logging.info(f"Time Taken for two threads : {end}")

    start = time.time()
    x = multiprocessing.Process(target=longCalculation, args=(count//2,))
    y = multiprocessing.Process(target=longCalculation, args=(count//2,))
    x.start()
    y.start()
    x.join()
    y.join()
    end = time.time() - start
    logging.info(f"Time Taken for two processes : {end}")

    