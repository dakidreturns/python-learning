import threading
import logging
import time

class FakeDatabase:
    def __init__(self):
        self.value = 0
        self._lock_a = threading.Lock()
        self._lock_b = threading.Lock()

    def a_then_b(self):
        logging.info("Waiting for lock A")
        self._lock_a.acquire()
        logging.info("Lock A aquired ")
        time.sleep(1)
        logging.info("Waiting for Lock B")
        self._lock_b.acquire()
        logging.info("Releasing Lock A")
        self._lock_a.release()
        logging.info("Releasing Lock B")
        self._lock_b.release()

    def b_then_a(self):
        logging.info("Waiting for lock B")
        self._lock_b.acquire()
        logging.info("Lock B aquired ")
        time.sleep(1)
        logging.info("Waiting for Lock A")
        self._lock_a.acquire()
        logging.info("Lock A aquired ")
        logging.info("Releasing Lock B")
        self._lock_b.release()
        logging.info("Releasing Lock A")
        self._lock_a.release()


    def update(self, name):
        logging.info("Thread %s: starting update", name)
        logging.debug("Thread %s about to lock", name) 
        with self._lock:
            local_copy = self.value
            local_copy += 1
            time.sleep(0.1)
            self.value = local_copy
        logging.debug("Thread %s after release", name)
        logging.info("Thread %s: finishing update", name)

if __name__ == "__main__":
    format='%(levelname)s : %(asctime)s Thread: %(threadName)s %(message)s '
    logging.basicConfig(format=format, level=logging.INFO,datefmt="%H:%M:%S")

    object = FakeDatabase()
    x = threading.Thread(target=object.a_then_b)
    y = threading.Thread(target=object.b_then_a)
    x.start()
    y.start()
