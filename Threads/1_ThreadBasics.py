import logging
import threading
import time

def thread_function(name, sleepTime):
    logging.info(f"Thread {name}: starting")
    time.sleep(sleepTime*10)
    logging.info(f"Thread {name}: finishing")


if __name__=="__main__":
    format  = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,datefmt="%H:%M:%S")

    logging.info("Main    : before creating thread")
    x = threading.Thread(target=thread_function, args=(1,5), daemon=True,)
    y = threading.Thread(target=thread_function, args=(2,2))
    logging.info("Main    : before running thread")
    x.start()
    y.start()
    logging.info("Main    : wait for thread to finish")
    # x.join()
    logging.info("Main    : All done")
