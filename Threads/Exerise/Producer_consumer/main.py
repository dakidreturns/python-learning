import logging

from threading import Event
from queue import Queue
import threading

from utils.helpers import DisplayDataInDatabase
from thread_workers.pipe_reader import pipe_reader
from thread_workers.ui_thread import UI_Thread
from utils.constants import LOGFORMAT
from utils.file_handler import FileHandler


if __name__ == "__main__":
    logging.basicConfig(format=LOGFORMAT, level=logging.INFO, datefmt='%H:%M:%S')

    threadDeadEvent = Event()
    pipeline = Queue()
    file_handler = FileHandler()

    database = list()

    reader_thread = threading.Thread(name = "Reader-Thread",target=pipe_reader, args=(pipeline, threadDeadEvent,file_handler))
    UI_thread = threading.Thread(name = "UI-Thread",target=UI_Thread, args=(pipeline, threadDeadEvent, file_handler))

    UI_thread.start()
    reader_thread.start()

    UI_thread.join()
    reader_thread.join()

    print("\n")
    # DisplayDataInDatabase(database)
