
class InvalidAgeException(Exception):
    "Raised when the input value is less than 0"
    def __str__(self):
        return "Age cant be less than 0"
    pass