class Person:    
    def __init__(self,name, age):
        self.name = name
        self.age = age
    def __str__(self):
        return f"Name: {self.name} Age: {self.age}"
    
    def containsNone(self):
        '''
        Returns true if any of the fields contain a None value.
        '''
        return (self.name == None or self.age == None)