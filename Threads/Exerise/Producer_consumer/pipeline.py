from queue import Queue 
import time

class Pipeline(Queue):
    def dump_data_to_file(self):
        '''
            Enter the current data to file.
            Prevent creation of new data
        '''
        with self.mutex:
            '''
                Blocks all read / write opeartion in the thread.
            '''
            time.sleep(3)
            pass
        pass


if __name__=="__main__":
    import threading
    import queue

    q = queue.Queue()

    def worker():
        while True:
            item = q.get()
            print(f'Working on {item}')
            print(f'Finished {item}')
            q.task_done()

    # Turn-on the worker thread.
    threading.Thread(target=worker, daemon=True).start()

    # Send thirty task requests to the worker.
    for item in range(30):
        q.put(item)

    # Block until all tasks are done.
    q.join()
    print('All work completed')