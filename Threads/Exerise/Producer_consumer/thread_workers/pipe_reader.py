from queue import Queue, Empty
from threading import Event
import logging
from utils.file_handler import FileHandler
from utils.constants import DATABASEFILE

def pipe_reader(pipeline:Queue,threadDead:Event,file_handler:FileHandler):
    '''
        Parameters:
            database: Database to which data is to be stored.
            pipeline: A queue which has item from where it reads data.
            event: threading.Event() to notify producer thread it took item from queue.
        Returns:
            None.
    '''
    count = 0
    while(True):
        if(threadDead.is_set()):
            break
        
        try:
            person = pipeline.get(timeout=1)
        except Empty:
            logging.debug("No data recieved, Reader sad!")
            continue

        file_handler.write_object_to_file(DATABASEFILE,person)
        
        logging.info(f"Got person: {person} from pipe" )
        
        logging.debug(f"Recived {count} amount of persons")
        
        logging.debug(f"Pipe Status: {pipeline.not_empty}")
        count += 1
    logging.info("Reader exiting.")