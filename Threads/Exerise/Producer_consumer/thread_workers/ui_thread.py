import logging
from queue import Queue
from threading import Event

from utils.file_handler import FileHandler
from utils.exception import InvalidAgeException

from utils.helpers import get_person_info
from thread_workers.menu import menu
from thread_workers.search import search
from utils.helpers import DisplayDataInDatabase
'''
Creates the person object and sends it to the queue
'''

def pipe_writer(pipeline:Queue):
    try:
        person = get_person_info()
    except:
        raise
    # except EOFError:
    #     threadDeadEvent.set()
    #     break
    # except (InvalidAgeException, ValueError):
    #     continue

    if(person == None or person.containsNone()):
        raise ValueError

    logging.info(f"Adding person {person} to pipe" )
    pipeline.put(person)

def UI_Thread(pipeline:Queue, threadDeadEvent:Event, file_handler:FileHandler):
    '''
        Parameters:
            event: threading.Event() to notify consumer thread an item is in the queue.
        Returns:
            None.
    '''
    while(True):
        if(threadDeadEvent.is_set()):
            logging.warn("Thread is dead....")
            break

        try:
            choice =  menu()
        except EOFError:
            threadDeadEvent.set()
            break
        except ValueError:
            continue
        
        if(choice == 1):
            try:
                pipe_writer(pipeline)
            except EOFError:
                threadDeadEvent.set()
                break
            except (InvalidAgeException, ValueError):
                continue
        elif(choice == 2):
            items = search(file_handler)
            if(len(items)==0):
                print("No user found")
            else:
                DisplayDataInDatabase(items)
        elif(choice == 3):
            threadDeadEvent.set()
            break

    logging.info("UI-thread exiting")

if __name__ == "__main__":
    pass