import logging
from threading import Condition
import time

def show_choice()->None:
    print("1. Enter User  ")
    print("2. Search User ")
    print("3. Exit")
    print("Enter Choice",end=" ")

def get_menu_input()->int:
    try:
        choice = input()
    except EOFError as e:
        raise e
    try:
        choice_int = int(choice)
        if(choice_int < 1 or choice_int > 3):
            raise ValueError
    except ValueError as e:
        logging.error("Enter integer values between 1 and 3")
        raise e
    return choice_int

def menu():
    show_choice()
    try:
        choice = get_menu_input()
    except EOFError as e:
        raise e
    except ValueError as e:
        raise e    
    return choice


        

        
