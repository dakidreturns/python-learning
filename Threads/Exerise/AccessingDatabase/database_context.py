'''
Reading and writing to database is done here.
'''

import sqlite3

from Threads.Exerise.AccessingDatabase.person import Person


def get_connection(database_name:str):
    return sqlite3.connect(database_name)

def create_table(connection:sqlite3.Connection,obj: object):
    pass

if __name__ == "__main__":
    con = sqlite3.connect("Example.db")
    cur = con.cursor()
    person = Person('name',21)
    print(person.__dict__())