import logging
import sys
import concurrent.futures
from concurrent.futures import as_completed

MAX_THREADS = 5

def row_range_multiply(starting_row:int,ending_row:int,matrix_a,matrix_b)->list:
    row_range_result = []
    for i in range(starting_row,ending_row):
        row_range_result.append([])
        for j in range(len(matrix_b[0])):
            row_range_result[i-starting_row].append(0)
            for k in range(len(matrix_b)):
                row_range_result[i-starting_row][j] += matrix_a[i][k] * matrix_b[k][j]
    return {"starting_row":starting_row, 
            "ending_row":ending_row,
            "result":row_range_result}

def get_intervals(n:int)->list:
    '''
    Parameters
        n: Is the total number of items in the range.
    Returns:
        A list of intervals with start index and end index
    '''

    if(MAX_THREADS>n):
        items_in_interval = 1
    else:
        items_in_interval = n/MAX_THREADS

    # items_in_interval = math.ceil(n/MAX_THREADS) if n>MAX_THREADS else n
    number_of_intervals = int(n/items_in_interval) # these gives the number of threads
    intervals = []
    i=-items_in_interval
    while(i := i+items_in_interval) < n:
        intervals.append((i,min(i+items_in_interval,n)))

    return intervals
    

if __name__ == "__main__":
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO, datefmt="%H:%M:%S")
    m = int(input())
    n = int(input())
    
    p = int(input())
    q = int(input())
    
    if(p != n):
        sys.exit()

    matrix_a = []
    matrix_b = []
    

    for i in range(m):
       matrix_a.append(list(map(int,input().split(' '))))
    
    for i in range(p):
       matrix_b.append(list(map(int,input().split(' '))) )
    answer = list()

    ''' 
        result will be an m * p matrix....  
        divide matrix_a into 5 row groups if possible
    '''

    with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
        futures = [executor.submit(row_range_multiply,start,end, matrix_a,matrix_b) for start,end in get_intervals(m)]

    for future in as_completed(futures):
        temp_result = future.result()
        start_index = temp_result['starting_row']
        end_index = temp_result['ending_row']
        for i in range(start_index, end_index ):
            answer.insert(i,temp_result['result'][i-start_index])
        logging.debug(temp_result)
    result = row_range_multiply(0,m,matrix_a,matrix_b)
    logging.debug(result)
    logging.info(f"Final answer using multi threadding: {answer} ")
    # print(get_intervals(2))
    # print(get_intervals(3))
    # print(get_intervals(5))
    # print(get_intervals(20))