import logging
from queue import Queue
import random
from threading import Event
import concurrent.futures
import time

'''
Creating a custom class for queue and giving max_size argument prevents
any more put operation until there is less item than max_size in the queue
till then the put operation will be blocked.
'''

def producer(pipe:Queue,event:Event):
    while not event.is_set():
        message = random.randint(0,100)
        logging.debug(f"Producer about to put {message} in pipe")
        pipe.put(message)
        logging.debug(f"Producer inserted {message} in pipe")

def consumer(pipe:Queue, event:Event):
    while not event.is_set() or not pipe.empty():
        logging.debug(f"Consumer taking item from queue")
        message = pipe.get()
        logging.debug(f"Consumer got message: {message} from pipe")

if __name__=="__main__":
    format = "%(asctime)s: %(message)s"
    
    logging.basicConfig(format=format, level=logging.DEBUG, datefmt="%H:%M:%S")
    
    pipe = Queue(maxsize=10)
    event = Event()

    with concurrent.futures.ThreadPoolExecutor(max_workers=2) as exector:
        exector.submit(producer, pipe,event)
        exector.submit(consumer, pipe,event)

        time.sleep(0.1)
        event.set()