'''
Producer consumer using Lock.
'''
import logging
import random
import threading
import concurrent.futures


class Pipeline:
    def __init__(self):
        self.message = 0
        self.producer_lock = threading.Lock()
        self.consumer_lock = threading.Lock()
        self.consumer_lock.acquire() # dont let consumer read initially
    
    def get_message(self, name):
        logging.debug(f"{name} about to acquire get-lock")
        self.consumer_lock.acquire()
        logging.debug(f"{name} acuired get-lock")
        message = self.message
        logging.debug(f"{name} about to release set-lock")
        self.producer_lock.release()
        logging.debug(f"{name} released set-lock") #producer can now set the value.
        return message
    
    def set_message(self, message,name):
        logging.debug(f"{name} about to acquire set-lock")
        self.producer_lock.acquire()
        logging.debug(f"{name} acuired set-lock")
        self.message = message
        logging.debug(f"{name} about to release get-lock")
        self.consumer_lock.release()
        logging.debug(f"{name} released get-lock") #producer can now set the value.
        return message

# an object to notify whether the producer has stopped
SENTINAL = object()

def producer(pipeline):
    '''
       Creates 10 random values and puts it to the pipe.
    '''
    for index in range(10):
        message = random.randint(1,100)
        logging.info(f"Producer got message {message}")
        pipeline.set_message(message,"Producer ")
    pipeline.set_message(SENTINAL,"Producer")        

def consumer(pipeline):
    '''
        Takes values in the pipe and displays it.
    '''
    message = 0
    while message is not SENTINAL:
        message = pipeline.get_message("Consumer")
        if message is not SENTINAL:
            logging.info(f"Consumer storing message {message}")

if __name__ == "__main__":
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")
    # logging.getLogger().setLevel(logging.DEBUG)

    pipeline = Pipeline()
    with concurrent.futures.ThreadPoolExecutor(max_workers=2) as executor:
        executor.submit(producer, pipeline)
        executor.submit(consumer, pipeline)