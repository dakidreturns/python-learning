 
LOGGING_FORMAT = "[%(levelname)s] %(asctime)s %(threadName)s : %(message)s"

'''
Logging level in python
    NOTSET=0.
    DEBUG=10.
    INFO=20.
    WARN=30.
    ERROR=40.
    CRITICAL=50.
'''
LOG_LEVEL =30

LOGGING_INTERFACE="Console"
DEFAULT_LOGGING_PORT = '/dev/ttyS4'
DATETIME_FORMAT = "%H:%M:%S"

HOST = "<the required ip>"
PORT = 8080
TIMEOUT = 10
MAX_RETRY = 3

SERIAL_ENCODING='utf-8'
SERIAL_INPUT_DATA_SEPERATOR = '|'
SERIAL_COMMAND_SEPERATOR  = " "
SERIAL_COMMAND_DELIMITER = '\n'

'''
Fully qualified path to the database file.
'''
DATABASE_FILE = r"<path to Data.json>"

'''
The names of tables in the database.
'''
TABLES_IN_DATABASE = ["table_1", "table_2"]

'''
Serial Port where communication takes place
'''
DEFAULT_SERIAL_PORT = '/dev/ttyS1'
