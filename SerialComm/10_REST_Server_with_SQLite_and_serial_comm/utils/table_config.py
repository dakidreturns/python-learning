COLUMNS = 'columns'
TYPE = 'type'
AUTOINCREMENT = 'auto_increment'
PRIMARY_KEY = 'primary_key'

TABLE_METADATA = {
    'person_data':{ 
        COLUMNS:{
            'id' : {
                TYPE: 'INTEGER',
                AUTOINCREMENT:True
            },
            'name' : {
                TYPE: 'VARCHAR(20)',
            },
            'age' : {
                TYPE : 'INTEGER'
            }
        },
        PRIMARY_KEY: 'id'
    },
    'employee_data':{
        COLUMNS:{
            'empid':{
                TYPE:'INTEGER'
            },
            'name' : {
                TYPE: 'VARCHAR(20)'
            },
            'experience':{
                TYPE: 'DECIMAL(3,1)'
            },
            'phone_no':{
                TYPE: 'INTEGER'
            }
        },
        PRIMARY_KEY: 'empid'
    }
}


def get_table_primary_key(table_name:str):
    return TABLE_METADATA.get(table_name)[PRIMARY_KEY]