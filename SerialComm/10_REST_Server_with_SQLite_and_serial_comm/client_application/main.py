import sys
import serial
import argparse
import threading
import time
from menu import Menu

dead_event = threading.Event()

def set_employee_data():
    print("Name : ", end="")
    name = input()
    print("ID : ", end="")
    id = input()
    print("Expericence : ", end="")
    experience = input()
    print("Phone_no : ", end="")
    phone_no = input()
    return f"set employee_data {name}|{id}|{experience}|{phone_no}\n"

def get_employee_data():
    print("Command To get Data: ")
    command = input()
    return command+"\n"

def handle_choice(choice,device):
    if choice == 1:
        command = set_employee_data()  
        device.write(command.encode('utf-8'))
    elif choice == 2:
        command = get_employee_data()
        device.write(command.encode('utf-8'))
    elif choice == 3:
        dead_event.set()
        sys.exit(0)

def sender(serial_port):
    while dead_event.is_set() is False:
        try:
            time.sleep(0.2)
            choice = menu.run_menu()
        except EOFError:
            dead_event.set()
            sys.exit(0)
        handle_choice(choice,serial_port)

def listener(serial_port:serial.Serial):
    while dead_event.is_set() is False:
        data = serial_port.read_until(b"\n")
        if data == b'':
            continue
        print(data.decode('utf-8'), end="")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-D", "--device", help="Path to device COM / tty ", required=True)
    menu = Menu([
        "Enter New Data",
        "Run Command",
        "Exit"
    ])

    args = vars(parser.parse_args())
    device = args["device"]
    ser = serial.Serial(device, baudrate=115200,timeout=1)
    dead_event.clear()
    
    listener_thread  = threading.Thread(target=listener, args=(ser,))
    sender_thread = threading.Thread(target=sender, args=(ser,))

    listener_thread.start()
    sender_thread.start()

    listener_thread.join()
    sender_thread.join()
    