import logging
import serial
import http.server 
import threading
import argparse
import time
import sys 
import serial.serialutil

import service.HTTPRequestHandler as HTTPRequestHandler
from service.serial_manager import SerialManager
import utils.constants as constants
from service.serial_port_logger import SerialPortLoggerStream

def init_cmdline_args() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser()

    parser.add_argument("-con", "--console", dest="console", help="Console to determine the interface for communication", choices=["Standard","Serial"], required=False)
    
    parser.add_argument("-d", "--device", dest="device", help="device where serial communication takes place", required=False)
    parser.add_argument("-bd", "--baud_rate", dest="baud_rate", help="baudrate of the serial device for serial communication",choices=[4800, 9600, 19200, 38400, 57600, 112500, 230400, 460800, 921600], required=False )
    parser.add_argument("-bs", "--byte-size", dest="byte_size", help="Number of bits for communication", choices=[5,6,7,8], required=False)
    
    parser.add_argument("-li", "--log-interface", dest="log_interface", help="Device where logging data is sent.", choices=["Console", "Serial", "File"],required=False)
    parser.add_argument("-lf", "--log-file", dest="log_file", help="File where logging data is sent. Provide only if LOG_INTERFACE is Serial", required=False)
    
    parser.add_argument("--data", dest="data", help="The maximum amount of data", required=False)
    
    return parser

def get_args(parser:argparse.ArgumentParser):
    '''
    Takes command line arguments and returns a tuple containig
    '''
    args = vars(parser.parse_args())



    try:
        if (args['console'] == "Standard"):
            output_file = "stdout"
        elif (args['console'] == "Serial"):
            device_name = args["device"]
            constants.DEFAULT_SERIAL_PORT = device_name

        if(args['log_interface'] == "Serial"):
            constants.LOGGING_INTERFACE = args['log_interface']
            constants.DEFAULT_LOGGING_PORT = args['log_file']

    except KeyError as e:
        print(parser.print_help(sys.stderr))
        print(e.__str__() + " Is required")
        sys.exit(1)

    return args

def configure_logger(args:dict):
    logging_args = {
        "format":constants.LOGGING_FORMAT,
        "datefmt" : constants.DATETIME_FORMAT,
        "level":constants.LOG_LEVEL,
    }
    if constants.LOGGING_INTERFACE == "Serial":
        ser = serial.Serial(port=constants.DEFAULT_LOGGING_PORT,baudrate=115200, bytesize=8, parity=serial.PARITY_NONE,stopbits=1, timeout=None)
        serial_stream = SerialPortLoggerStream(ser)
        logging_args["stream"] = serial_stream
    
    logging.basicConfig(**logging_args)

def create_server():
    logging.info(f"Starting Server at: ({constants.HOST}:{constants.PORT}) ")
    server = http.server.HTTPServer((constants.HOST,constants.PORT), HTTPRequestHandler.HTTPRequestHandler)
    
    return server

def create_serial_manager():
    try:
        serial_port = serial.Serial(port=constants.DEFAULT_SERIAL_PORT,baudrate=115200, bytesize=8, parity=serial.PARITY_NONE,stopbits=1, timeout=1)
    except serial.serialutil.SerialException as e:
        logging.error(e.__str__())
        sys.exit(1)
    serial_manager = SerialManager(serial_port)

    return serial_manager

def manage_server(server:http.server.HTTPServer):
    server.serve_forever()

def serial_handler(serial_manager: SerialManager):
    serial_manager.listen_forever()

if __name__ == "__main__":
    parser = init_cmdline_args()
    args = get_args(parser)
    
    configure_logger(args)

    serial_manager = create_serial_manager()

    server = create_server()
        
    server_thread = threading.Thread(target=manage_server, args=(server,))
    serial_thread = threading.Thread(target=serial_handler,args=(serial_manager,))
    
    serial_thread.start()
    server_thread.start()
    
    try: 
        while True:
            input()
    except (EOFError,KeyboardInterrupt):
        server.shutdown()
        server.server_close()
        serial_manager.shutdown()