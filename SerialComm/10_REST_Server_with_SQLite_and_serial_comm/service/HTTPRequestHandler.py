import http.server
import logging
import json
import re

import utils.helpers
from interface.request_handeler_interface import RequestHandlerInterface
from service.regex_creator import UrlRegexCreator

QUERIES = ["person_data","employee_data"]
PARAMETERS = {"id": "numeric", "empid":"numeric"}

class HTTPRequestHandler(http.server.BaseHTTPRequestHandler):

    def request_forbidden(self):
        '''
            Sends a 403 response to client when the client
            sends request to invalid url
        '''
        self.send_error(http.HTTPStatus.FORBIDDEN)
        self.end_headers()


    def error_occured(self, msg:str):
        '''
            Send generic error to client incase of failure. 
            Doesn't log the error sepeatly in the server side.
        '''
        self.send_error(http.HTTPStatus.INTERNAL_SERVER_ERROR,msg)
        self.end_headers()


    def resouce_not_found(self,):
        self.send_error(http.HTTPStatus.NOT_FOUND)
        self.end_headers()


    def bad_request(self):
        self.send_error(400)
        self.end_headers()


    def send_Json_header(self, response_code):
        '''
            Send Content-Type : application/json
            header to client
        '''
        self.send_response(response_code)
        self.send_header('Content-Type','application/json')
        self.end_headers()


    def write_response_body(self, data:bytes):
        self.wfile.write(data)


    def send_data(self,data):
        if(data is None):
            self.resouce_not_found()
            return
        data_json = json.dumps(data, default=utils.helpers.obj_dict)
        self.send_Json_header(http.HTTPStatus.OK)
        self.write_response_body(data_json.encode('utf-8'))


    def get_json_data_from_request(self)->dict:
        '''
            When we a are expecting a person data from client
            run this function and get the person object
            
            Returns:
                ``data_object``: a dict object containing the key value pairs of the incoming data  
        '''
        data_length = int(self.headers.get('Content-Length'))
        logging.debug(f"Client sends data of length:{data_length}")
        if(data_length == 0):
            return None
        
        try:
            data = self.rfile.read(data_length).decode('utf-8')
            data_object = dict(**json.loads(data))
        except json.JSONDecodeError as e:
            logging.debug(f"Error parsing JSON: {e.msg}"  )
            return None

        
        return data_object

    
    def do_GET(self):
        logging.debug(f"GET Request recived: {self.path}")
        
        regex_string = UrlRegexCreator.get_regex(self.command,QUERIES,PARAMETERS)
        logging.debug(f"Regex String created: {regex_string}")
        regex_match = re.match(regex_string,self.path)

        if(regex_match is None):
            self.request_forbidden()
            return
        
        regex_match_dict = regex_match.groupdict()
        logging.debug(f"{regex_match.groupdict()}")
        try:
            data = RequestHandlerInterface.get_data(regex_match_dict)
        except Exception as e:
            logging.error(f"Error occured {e.__str__()}, \n{e.__traceback__}")
            self.error_occured("Error in fetching data")
            return
        self.send_data(data)
        
        
    def do_POST(self):
        regex_string = UrlRegexCreator.get_regex(self.command,QUERIES,PARAMETERS)
        regex_match = re.match(regex_string,self.path)

        if(regex_match is None):
            self.request_forbidden()
            return 
        
        request_dict = regex_match.groupdict()
       
        try:
            data_object = self.get_json_data_from_request()
        except json.decoder.JSONDecodeError:
            self.bad_request()
            return
        
        logging.debug(f"{data_object}")

        try:
            response = RequestHandlerInterface.add_item(request_dict=request_dict,json_data=data_object)     
        except Exception as e: 
            self.error_occured("Error occured during addition")
            logging.error(e.__str__())
            return 
       
        if response is not None:
            self.send_response(http.HTTPStatus.CREATED)
        else:
            self.error_occured("Error occured during addition")
            return
        self.end_headers()

    
    def do_PUT(self):
        regex_string = UrlRegexCreator.get_regex(self.command,QUERIES,PARAMETERS)
        regex_match = re.match(regex_string,self.path)
        
        logging.debug(f"{regex_match}")
        if(regex_match is None ):
            self.request_forbidden()
            return
        logging.debug(f"{regex_match.groups()}")
        
        request_dict = regex_match.groupdict()

        data_object = self.get_json_data_from_request()
        
        if data_object is None :
            self.bad_request()
            return 
        try:
            updated_item = RequestHandlerInterface.update_item(request_dict,data_object)
        except Exception as e:
            logging.error(f"Error occured {e.__str__()}")
            self.error_occured("Error during getting data")
            return
        
        if(updated_item is None):
            self.resouce_not_found()
            return
        self.send_data(updated_item)

    def do_DELETE(self):
        regex_string = UrlRegexCreator.get_regex(self.command,QUERIES,PARAMETERS)
        regex_match = re.match(regex_string,self.path)
        
        if(regex_match is None):
            self.request_forbidden()
            self.send_error(403)
            return
        
        request_dict = regex_match.groupdict()
        
        logging.debug(f"{regex_match.groups()}")

        try:
            deleted_data = RequestHandlerInterface.delete_item(request_dict)
        except Exception as e:
            logging.error(f"Error occured {e.__str__()}")
            self.error_occured("Error during Deletion")
            return
        
        if deleted_data is None:
                self.resouce_not_found()
                return
        self.send_response(http.HTTPStatus.ACCEPTED)
        self.end_headers()


    def log_message(self, format, *args):
        message = format % args
        logging.info(f"{self.address_string()} - - {message}" )