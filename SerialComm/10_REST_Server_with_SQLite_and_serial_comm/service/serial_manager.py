import serial
import threading
import logging

from interface.serial_command_interface import SerialComandInterface
from utils.constants import SERIAL_ENCODING,SERIAL_INPUT_DATA_SEPERATOR, SERIAL_COMMAND_SEPERATOR, SERIAL_COMMAND_DELIMITER

N_ROWS_AFFECTED = " Rows affected"
SET_ARGUMENTS_INDEX = 0


class SerialManager:
    def __init__(self,serial_port:serial.Serial):
        self.serial_port = serial_port
        self.__shutdown_request = threading.Event()
        
    def set(self,data_set, args:list[str]):
        '''
            args format is a list of strings of which we want the first
        '''
        (status, reason) = SerialComandInterface.add_data(data_set,args[SET_ARGUMENTS_INDEX].split(SERIAL_INPUT_DATA_SEPERATOR))
        reason += SERIAL_COMMAND_DELIMITER
        self.serial_port.write(reason.encode(SERIAL_ENCODING))

    def get(self, data_set, args:list[str]):
        (status, reason, data) = SerialComandInterface.get_data(data_set, args)
        logging.info(reason)
        logging.debug(f"Sending data: {data}")
        reason += SERIAL_COMMAND_DELIMITER
        if status == False:
            self.serial_port.write(reason.encode(SERIAL_ENCODING))
            return
        self.serial_port.write(f"{data}{SERIAL_COMMAND_DELIMITER}".encode(SERIAL_ENCODING))

    def delete(self, data_set,args:list[str]):
        (status, reason, rows_affected) = SerialComandInterface.delete_data(data_set, args)
        reason += SERIAL_COMMAND_DELIMITER
        if(status == False):
            self.serial_port.write(reason.encode(SERIAL_ENCODING))
            return
        self.serial_port.write(f"{rows_affected} {N_ROWS_AFFECTED}".encode(SERIAL_ENCODING))

    def process_data(self,data:str):
        '''
        Accepts the command in data variable
        the command is of the form:

        `<action> <db name> <args>`
        
        this fuction will call the corresponding `action` (if it is defined). 
        '''
        data = data.strip()
        data_list = data.split(SERIAL_COMMAND_SEPERATOR)

        try:
            command = data_list[0].lower()
            data_set = data_list[1]
            args = data_list[2:]
        except IndexError:
            logging.error(f"Index out of range")
            self.serial_port.write(f"Wrong number of arguments\n".encode(SERIAL_ENCODING))
            return
        
        logging.debug(f"Command: {command}, Querying db: {data_set}, arguments: {args}")

        if not hasattr(self, command):
            logging.error(f"Method not implemented {command}")
            return
        method = getattr(self,command)
        method(data_set,args)

    def listen_forever(self):
        while  self.__shutdown_request.is_set() == False:
            data = self.serial_port.read_until(bytes(SERIAL_COMMAND_DELIMITER,SERIAL_ENCODING))
            data = data.decode(SERIAL_ENCODING)
            if(data == ""):
                continue
            logging.debug(f"Data recived : {data}")
            self.process_data(data)
    
    def shutdown(self):
        self.__shutdown_request.set()