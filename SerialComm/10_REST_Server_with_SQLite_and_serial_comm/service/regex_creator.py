# import utils.constants as constants
try:
    from http import HTTPMethod
    GET = HTTPMethod.GET
    POST = HTTPMethod.POST
    PUT = HTTPMethod.PUT
    DELETE = HTTPMethod.DELETE

except (ImportError, NameError): # Prior to python 3.11
    GET = 'GET'
    POST = 'POST'
    PUT = 'PUT'
    DELETE = 'DELETE'

DATA_TYPES = {"numeric" : "[0-9]+",
              "alphanumeric":"[0-9a-zA-Z]+"
              }

class UrlRegexCreator:
    @staticmethod
    def create_parameter_string(parameters_with_data_type:dict):
        r'''
        Function for creating parameter regex string of the form  
        `\?param_name_1=(?P<param_name_1>[0-9]+)|\?param_name_2=(?P<param_name_2>[0-9a-zA-Z]+)`
        
        ''' 
        parameter_string = "|".join([f"\\?{parameter}=(?P<{parameter}>{DATA_TYPES[datatype]})" for parameter,datatype in parameters_with_data_type.items()])
        return parameter_string
    
    @staticmethod
    def get_regex(method,queries:list, parameters:dict):
        r'''
        Create URL Regex according to the given parameters and queries.
        The generated Regex will capture the queries into a `query` key and the 
        parameter into the corresponding name given in the parameters dict.  
        
        Parameters:
            `method`: The Corresponding HTTP method called.
            `queries`: List of queries that needs to be captured.
            `parmeters` : a `dict` containing the parameters to be captured along with the data types
                                defined in `regex_creator.DATA_TYEPS`.

        Sample Input:
            method = "GET",  
            queries = ['person_data','sample_data' ],  
            parameters = {'id':'numeric','uid': 'alphanumeric'}  

        Sample Output:
            ^/(?P<query>person_data|sample_data)(/|\?id=(?P<id>[0-9]+)|\?uid=(?P<uid>[0-9a-zA-Z]+)/?)?$  
        There are 4 parts for this regex:
        1. `^/` -- Starts with '/'
        2. `(?P<query>person_data|sample_data)` -- capture `person_data` or `sample_data` into named group `query`
        3. `(/|\?id=(?P<id>[0-9]+)|\?uid=(?P<uid>[0-9a-zA-Z]+)/?)?` --  match ending '/' or '?id=<numeric>' or '?uid=<alphanumer>' if it exists (denoted by `\?`) 
        4. `$` ending of line  -- Anything other than above character after the string will invalidate the regex.
        
        '''
        queries_string = '|'.join(queries)
        if(parameters != None):
            parameters_string = UrlRegexCreator.create_parameter_string(parameters)
        
        if(method == GET):
            regex_string = f"^/(?P<query>{queries_string})(/|{parameters_string}/?)?$"
        elif(method == POST):
            regex_string = f"^/(?P<query>{queries_string})/?$"
        elif(method == PUT or method == DELETE):
            regex_string = f"^/(?P<query>{queries_string})({parameters_string})/?$"
        return regex_string
    
if __name__ == "__main__":
    print(UrlRegexCreator.get_regex(GET,['person_data','sample_data' ],{'id':'numeric','uid': 'alphanumeric'}))