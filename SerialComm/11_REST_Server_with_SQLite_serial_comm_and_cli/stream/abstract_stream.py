from abc import ABC, abstractmethod

class AbstractStream(ABC):
    @abstractmethod
    def read(self) -> str :
        pass
    
    @abstractmethod
    def write(self,data: str)->int:
        pass