from sys import stdin

from stream.abstract_stream import AbstractStream
from utils import constants

class ConsoleStream(AbstractStream):
    def __init__(self):
        AbstractStream.__init__(self)
    
    def read(self, ) -> str:
        return input()
    
    def write(self, data: str) -> int:
        print(data, end="")
        return len(data)