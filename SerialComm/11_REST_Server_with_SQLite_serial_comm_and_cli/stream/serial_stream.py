import serial

from stream.abstract_stream import AbstractStream
from utils import constants

class SerialStream(AbstractStream):
    def __init__(self, serial: serial.Serial):
        AbstractStream.__init__(self)
        self.serial = serial

    def read(self, ) -> str:
        data = self.serial.read_until(constants.COMMAND_DELIMITER.encode(constants.SERIAL_ENCODING))
        return data.decode(constants.SERIAL_ENCODING)
    
    def write(self, data: str) -> int:
        data += constants.COMMAND_DELIMITER
        return self.serial.write(data.encode(constants.SERIAL_ENCODING))