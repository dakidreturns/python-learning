NO_CHOICE_RECIVED = 0

class Menu():
    def __init__(self,choice_list:list, stream):
        '''
        Parameters:
            ``choice_list`` : list of choices that need to be displayed to the user
        Returns:
            The choice selected by the user.
        This class displays a general menu with the items passed in ``choice_list`` as ``1...n``
        where ``n`` is the number of items in the ``choice_list``
        Raises ``ValueError`` exception if the user input is not in ``acc_inputs``. 
        '''
        self.choice_list:list = choice_list
        self.stream = stream

    def show_choice(self) -> None:
        for index, string in enumerate(self.choice_list):
            self.stream.write(f"{index+1}. {string}\n")
        self.stream.write("Enter Choice")

    def get_menu_input(self) -> int:
        choice = self.stream.read()
        if(choice == ""):
            return NO_CHOICE_RECIVED
        try:
            choice_int = int(choice)
        except ValueError as e:
            raise e
        if(choice_int<1 and choice_int>len(self.choice_list)):
            raise ValueError("Entered integer value is out of range")
        return choice_int

    def run_menu(self):
        self.show_choice()
        choice = self.get_menu_input()
        return choice