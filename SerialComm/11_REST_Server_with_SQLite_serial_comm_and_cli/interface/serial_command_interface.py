import logging
import service.database_service as dbService

GOOD_ACK = "GOOD"
BAD_ID = "Entered Id is not valid"
BAD_EXP = "Experience Value must be float"
BAD_PHONE = "Phone number must have 10 digits"
DATA_NOT_FOUND = "The data requested does not exist"

EMPLOYEE_ADD_COMMAND_NOT_ENOUGH_DATA = "Syntax : set employee_data name|empid|exp|phone_no"
EMPLOYEE_ADD_COMMAND_NAME_INDEX = 0
EMPLOYEE_ADD_COMMAND_ID_INDEX = 1
EMPLOYEE_ADD_COMMAND_EXP_INDEX = 2
EMPLOYEE_ADD_COMMAND_PHONE_INDEX = 3

GET_DELETE_COMMAND_COLUMN_INDEX = 0
GET_DELETE_COMMAND_DATA_ITEM_INDEX = 1

ARGUMENT_NOT_FOUND = "One or more Argument is missing"

class SerialComandInterface:
    @staticmethod
    def check_employee_data(data_list:list[str]):
        data_dict = {}
        try:
            name = data_list[EMPLOYEE_ADD_COMMAND_NAME_INDEX]    
            try:
                empid = int(data_list[EMPLOYEE_ADD_COMMAND_ID_INDEX])
            except ValueError:
                return (False, BAD_ID, data_dict)

            try:
                exp = float(data_list[EMPLOYEE_ADD_COMMAND_EXP_INDEX])
            except ValueError:
                return (False, BAD_EXP, data_dict)
            
            phone_no = data_list[EMPLOYEE_ADD_COMMAND_PHONE_INDEX]
            if(len(phone_no) != 10):
                return (False, BAD_PHONE,data_dict)
            if phone_no.isalpha():
                return (False, BAD_PHONE,data_dict)
        
        except IndexError:
            return (False,EMPLOYEE_ADD_COMMAND_NOT_ENOUGH_DATA, data_dict)
        
        data_dict = {
            "name" : name,
            "empid" : empid,
            "experience" : exp,
            "phone_no" : phone_no
        }

        return (True,GOOD_ACK, data_dict)

    @staticmethod
    def prepare_employee_data(data):
        data_new = []
        for x in data:
            x["empid"] = f"{x['empid']:04}"
            x["experience"] = f"{x['experience']:.1f}"
            data_new.append(x)
        return data_new
    
    @staticmethod
    def add_data(table_name:str, data_list:list[str] ):
        status = True
        reason = GOOD_ACK
        
        check_method_name = f"check_{table_name}"
        if not hasattr(SerialComandInterface,check_method_name):
            return (False, f"Data verification for {table_name} not implemented")
        
        check_method = getattr(SerialComandInterface,check_method_name)
        status, reason, data_dict = check_method(data_list)
        logging.info(data_dict)
        
        if status == False:
            return (status,reason)
        
        try:
            dbService.dbContext.add_entry(table_name, data_dict)
        except Exception as e:
            status = False
            reason = e.__str__()
        return(status, reason)
    
    @staticmethod
    def get_data(table_name:str, arguments:list[str]):
        status = True
        reason = GOOD_ACK
        data = None
        try:
            column_name = arguments[GET_DELETE_COMMAND_COLUMN_INDEX]
            data_item = arguments[GET_DELETE_COMMAND_DATA_ITEM_INDEX]
        except IndexError:
            return (False,ARGUMENT_NOT_FOUND,None)
        data_item = f'"{data_item}"'

        logging.debug(f"Column_name: {column_name}")
        try: 
            data = dbService.dbContext.get_data_by_field(table_name,column_name, data_item)
        except Exception as e:
            status = False
            reason = e.__str__()
            return (status, reason, data)
        
        if data == None or data == {}:
            status = False
            reason = DATA_NOT_FOUND
            return (status, reason, data)
        
        # prepare the data according to requirements
        data_prepare_method_name = f"prepare_{table_name}"
        if hasattr(SerialComandInterface,data_prepare_method_name) == False:
            logging.warning(f"Sending Raw data from database. Inplement {data_prepare_method_name} to modify the data before sending to serial port")
        else:
            data_prepare_method = getattr(SerialComandInterface,data_prepare_method_name)
            data = data_prepare_method(data)
        return (status, reason, data)
    
    @staticmethod
    def delete_data(table_name:str, arguments:list[str]):
        status = True
        reason = GOOD_ACK
        try:
            column_name = arguments[GET_DELETE_COMMAND_COLUMN_INDEX]
            data_item = arguments[GET_DELETE_COMMAND_DATA_ITEM_INDEX]
        except IndexError:
            return (False,ARGUMENT_NOT_FOUND,None)
        
        data_item = f'"{data_item}"'
        rows_affected = None
        
        try:
            rows_affected = dbService.dbContext.delete_data_by_field(table_name, column_name,data_item)
        except Exception as e:
            status = False
            reason = e.__str__()
        return (status, reason, rows_affected)
        