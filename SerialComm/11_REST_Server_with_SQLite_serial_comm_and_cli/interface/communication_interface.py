import logging

from utils.menu import Menu
from interface.serial_command_interface import SerialComandInterface
from stream.abstract_stream import AbstractStream
from utils.constants import COMMAND_DELIMITER, COMMAND_INPUT_DATA_SEPERATOR, COMMAND_SEPERATOR


N_ROWS_AFFECTED = " Rows affected"
SET_ARGUMENTS_INDEX = 0

class CommunicationInterface:
    def __init__(self, stream: AbstractStream):
        self.stream = stream
        self.menu = Menu(["Enter Data",
                          "Get Data",
                          "Run Command",
                          "Exit"], self.stream)
        
    def set(self,data_set, args:list[str]):
        '''
            args format is a list of strings of which we want the first
        '''
        (status, reason) = SerialComandInterface.add_data(data_set,args[SET_ARGUMENTS_INDEX].split(COMMAND_INPUT_DATA_SEPERATOR))
        reason += COMMAND_DELIMITER
        self.stream.write(reason)

    def get(self, data_set, args:list[str]):
        (status, reason, data) = SerialComandInterface.get_data(data_set, args)
        logging.info(reason)
        logging.debug(f"Sending data: {data}")
        reason += COMMAND_DELIMITER
        if status == False:
            self.stream.write(reason)
            return
        self.stream.write(f"{data}{COMMAND_DELIMITER}")

    def delete(self, data_set,args:list[str]):
        (status, reason, rows_affected) = SerialComandInterface.delete_data(data_set, args)
        reason += COMMAND_DELIMITER
        if(status == False):
            self.stream.write(reason)
            return
        self.stream.write(f"{rows_affected} {N_ROWS_AFFECTED}")

    def run_command(self):
        command = self.stream.read()
        command = command.strip()

        command_list = command.split(COMMAND_SEPERATOR)

        try:
            command = command_list[0].lower()
            data_set = command_list[1]
            args = command_list[2:]
        except IndexError:
            logging.error(f"Index out of range")
            self.stream.write(f"Wrong number of arguments\n")
            return
        
        logging.debug(f"Command: {command}, Querying db: {data_set}, arguments: {args}")

        if not hasattr(self, command):
            logging.error(f"Method not implemented {command}")
            return
        method = getattr(self,command)
        method(data_set,args)

        pass
    def run_till_death(self):
        while True:
            choice = self.menu.run_menu()
            if choice == 3:
                self.run_command()
            if(choice == 4):
                break
    