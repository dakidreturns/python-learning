class Menu():
    def __init__(self,choice_list:list):
        '''
        Parameters:
            ``choice_list`` : list of choices that need to be displayed to the user
        Returns:
            A Menu object
        This class displays a general menu with the items passed in ``choice_list`` as ``1...n``
        where ``n`` is the number of items in the ``choice_list``
        Raises ``ValueError`` exception if the user input is not in ``acc_inputs``. 
        '''
        self.choice_list:list = choice_list

    def show_choice(self) -> None:
        for index, string in enumerate(self.choice_list):
            print(f"{index+1}. {string}")
        print("Enter Choice",end=" ")

    def __get_menu_input(self) -> int:
        '''
        Throws ``ValueError`` if erroneous input is passed
        '''
        try:
            choice = input()
        except EOFError as e:
            raise e
        choice_int = int(choice)
        
        if(choice_int<1 and choice_int>len(self.choice_list)): 
            raise ValueError("Entered integer value is out of range")
        return choice_int

    def run_menu(self):
        self.show_choice()
        try:
            choice = self.__get_menu_input()
        except Exception as e:
            raise e
        return choice