import serial
import sys

def handle_data(serial_port:serial.Serial , data:str):
    if type(data) is bytes:
        data = data.decode("utf-8")
    
    
    
    serial_port.write(data.encode('utf-8'))

if __name__ == "__main__":
    
    serial_port = serial.Serial(port='/dev/ttyS4',baudrate=115200, bytesize=8, parity=serial.PARITY_NONE,stopbits=1, timeout=None)
    while True:
        try:
            data = serial_port.read_until('\r'.encode('utf-8'))
        except KeyboardInterrupt:
            sys.exit(0)
        print(f"Data recived : {data}")
        handle_data(serial_port,data)