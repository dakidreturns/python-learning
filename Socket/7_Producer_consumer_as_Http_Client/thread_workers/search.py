import json
from utils.file_handler import FileHandler
from utils.constants import DATABASEFILE
from utils.helpers import convert_dict_to_person


def search(file_handler:FileHandler):
    print("Enter value to search ")
    text_to_search = input()
    items = file_handler.search_for_text(DATABASEFILE, text_to_search)
    return list(map(convert_dict_to_person,map(json.loads,items)))
