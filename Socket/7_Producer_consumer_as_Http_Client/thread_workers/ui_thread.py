import logging
import http.client
import threading

import utils.helpers
import utils.http_custom_connection

from queue import Queue

from utils.helpers import get_person_info
from thread_workers.menu import Menu
from thread_workers.search import search
from utils.helpers import DisplayDataInDatabase

'''
    Creates the person object and sends it to the queue
'''



class UI_Thread(threading.Thread):
    def __init__(self, name:str, pipeline:Queue, threadDeadEvent:threading.Event, httpClientConnection:utils.http_custom_connection.HTTPCustomConnction):
        threading.Thread.__init__(self,name=name)
        self.pipeline = pipeline
        self.threadDeadEvent = threadDeadEvent
        self.httpClientConnection = httpClientConnection
        
        self.menu = Menu([  "Enter User  ",
                            "Get all users",
                            "Get user by ID",
                            "Exit"
                        ])
        self.count = 0


    def pipe_writer(self):
        # while (True):
        #     person = utils.helpers.create_random_person()
        #     if(person == None or person.containsNone()):
        #         raise ValueError

        #     logging.info(f"Adding person {person} to pipe" )
        #     self.pipeline.put(person)
        #     self.count+=1
        person = get_person_info()
        if(person == None or person.containsNone()):
            raise ValueError

        logging.info(f"Adding person {person} to pipe" )
        self.pipeline.put(person)
        self.count+=1

    def send_request_to_server(self,method:str, url:str) -> http.client.HTTPResponse:
        # response =  self.httpClientConnection.send_request(method,url)
        self.httpClientConnection.request(method=method, url=url)
        response = self.httpClientConnection.getresponse()
        return response

    def get_all_data_from_server(self)->map:
        response = self.send_request_to_server("GET",r"/person_data")
        logging.debug(f"Server reply: {response.status} {response.reason}")
        
        if(response.status == 200):
            body = response.read().decode('utf-8')
        else:
            body = response.reason

        return {"status" : response.status,"body" : body}
    
    def data_corresponding_to_id(self,id)->map:
        response = self.send_request_to_server("GET",f"/person_data?id={id}")
        logging.debug(f"Server reply: {response.status} {response.reason}")
        
        if(response.status == 200):
            body = response.read().decode('utf-8')
        else:
            body = response.reason

        return {"status" : response.status,"body":body}

    def handle_choice(self, choice):
        if(choice == 1):
            self.pipe_writer()

        # elif(choice == 2):
        #     items = search(self.file_handler)

        #     if(len(items)==0):
        #         print("No user found")
        #     else:
        #         DisplayDataInDatabase(items)
        
        elif(choice == 2):
            response = self.get_all_data_from_server()
            if(response['status'] != 200):
                raise Exception(f"Couldn't recived completed data Server said: {response['body']}")
            else:
                print(response['body'])

        elif(choice == 3):
            print("Enter the user Id: ", end=" ")
            id = int(input())
            response = self.data_corresponding_to_id(id)
            if(response['status'] != 200):
                raise Exception(f"Couldn't recived completed data Server said: {response['body']}")
            else:
                print(response['body'])

        elif(choice == 4):
            self.threadDeadEvent.set()

    def run(self):


        while(True):
            if(self.threadDeadEvent.is_set()):
                logging.warn("Thread is dead....")
                break
            try:
                choice = self.menu.run_menu()
            except EOFError:
                self.threadDeadEvent.set()
                break
            except ValueError:
                logging.error(f"Enter integer values between 1 and {len(self.menu_choices)}")
                continue
            
            try:
                self.handle_choice(choice)
            except EOFError:
                self.threadDeadEvent.set()
            except Exception as e:
                logging.error(f"Exception occured : {e}")
            choice = -1
            
        logging.info(f"Added {self.count} number of persons")

if __name__ == "__main__":
    pass