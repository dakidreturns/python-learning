import http.client
import threading

class HTTPCustomConnction (http.client.HTTPConnection):
    def __init__(self, *args, **kwargs):
        '''
        Takes in the same parameters as http.client.HTTPConnection
        '''
        http.client.HTTPConnection.__init__(self,*args, **kwargs)
        self._mutex = threading.Lock()
    
    def send_request(self, method:str, url:str, body=None,  headers = {} ) -> http.client.HTTPResponse:
        '''
            Sends HTTP requst and returns the http.client.HTTPResponse object
        '''
        with self._mutex:
            self.request(method=method, url=url, body=body, headers=headers)
            response = self.getresponse()
        return response