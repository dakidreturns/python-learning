LOGGING_FORMAT = "[%(levelname)s] %(asctime)s %(threadName)s : %(message)s"
DATETIME_FORMAT = "%H:%M:%S"
DATABASEFILE="./database.txt"
SERVER_HOST = "192.168.2.175"
SERVER_PORT = 2389
TIMEOUT = 10
MAX_RETRY = 3