import json
import threading
import mmap
from collections.abc import Generator

class FileHandler():

    def __init__(self):
        self.__mutex = threading.Lock()
        self._can_read = threading.Condition(self.__mutex)

    def write_object_to_file(self,filename, object):
        '''
        Parameters: 
            filename: name of file to write to
            object : object to write (converts it to json)
        Returns: 
            None
        '''
        with self.__mutex:
            with open(filename,'a') as file:
                file.write(json.dumps(object.__dict__)+'\n')
                self._can_read.notify()


    def search_for_text(self, filename, text_to_search) -> Generator[str] :
        '''
        Parameters:
            filename: Name of file where the text to be searched.
            text_to_serach: 
        Returns:
            List containing all the lines of matching text.
        '''
        with self._can_read:
            lines =[]
            with open(filename, 'r') as file:
                for line in file.readlines():
                    if(line.find('"'+text_to_search+'"')!=-1):
                        yield line
            # return lines


    def search_for_text_mmap(self,filename,text_to_serch):
        '''
        Parameters:
            filename: Name of file where the text to be searched.
            text_to_serach: 
        '''
        with self._can_read:
            with open(filename, 'rb') as file:
                with  mmap.mmap(file.fileno(),length=0,access=mmap.ACCESS_READ, offset=0) as mm_file:
                    position = mm_file.find(bytes(text_to_serch,"utf-8"))
                    mm_file.seek(position-10)
                    print(str(mm_file.readline(),'utf-8'))
                    


def write_object_to_file(filename, object:str):
    '''
    Parameters: 
        filename: name of file to write to
        object : object to write (converts it to json)
    Returns: 
        None
    '''
    with open(filename,'a') as file:
        file.write(json.dumps(object.__dict__)+'\n')

if __name__ == "__main__":
    from constants import DATABASEFILE

    fileHandler = FileHandler()
    items = fileHandler.search_for_text(DATABASEFILE, "Rohit")
    print(items)