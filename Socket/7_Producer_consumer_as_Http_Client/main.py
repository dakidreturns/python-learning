import logging
import threading
import http.client

from queue import Queue

import utils.constants
import utils.http_custom_connection
from utils.helpers import DisplayDataInDatabase
from thread_workers.pipe_reader import PipeReader
from thread_workers.ui_thread import UI_Thread

from utils.file_handler import FileHandler


if __name__ == "__main__":
    logging.basicConfig(format=utils.constants.LOGGING_FORMAT, level=logging.INFO, datefmt='%H:%M:%S')

    threadDeadEvent = threading.Event()
    pipeline = Queue()
    file_handler = FileHandler()
    database = list()
    
    #Using multiple connection to server
    httpClientConnection_Consumer = http.client.HTTPConnection(utils.constants.SERVER_HOST,utils.constants.SERVER_PORT)
    httpClientConnection_Producer = http.client.HTTPConnection(utils.constants.SERVER_HOST,utils.constants.SERVER_PORT)
    
    #Using a single connection to server
    # httpClientConnection = http.client.HTTPConnection(utils.constants.SERVER_HOST,utils.constants.SERVER_PORT)

    reader_thread = PipeReader("Reader-Thread",pipeline,threadDeadEvent,httpClientConnection_Consumer)
    UI_thread     = UI_Thread ("UI-Thread"    ,pipeline,threadDeadEvent,httpClientConnection_Producer)

    UI_thread.start()
    reader_thread.start()

    UI_thread.join()
    reader_thread.join()

    print("\n")