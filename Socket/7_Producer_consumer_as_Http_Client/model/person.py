class Person:
    def __init__(
            self,
            id:int|None=None, 
            name:str|None=None, 
            age:int|None = None, 
            *args, 
            **kwargs
            ):
        self.id = id
        self.name = name
        self.age = age
        
    def __str__(self):
        return f"id: {self.id} Name:{self.name} Age:{self.age}"
    
    def __json__(self):
        return self.__dict__
    
    def containsNone(self):
        '''
        Returns true if any of the fields contain a None value.
        '''
        return (self.name == None or self.age == None)