import socket
import logging
import time

if __name__ == "__main__":
    HOST = "127.0.0.1"
    PORT = 34983
    #PORT = 34983
    MAX_TRIES = 3
    TIMEOUT = 10

    data = "This is a good sign"
    format = "[%(levelname)s] %(asctime)s : %(msg)s"
    time_format = "%H:%M:%S"
    retry_count = 0

    logging.basicConfig(format=format, datefmt=time_format, level=logging.INFO)

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client:
        while retry_count < MAX_TRIES:
            client.settimeout(TIMEOUT) # 10 second timeout
            try: 
                client.connect((HOST,PORT))
            except ConnectionRefusedError as e:
                logging.error("Connection refused: Retrying")
                retry_count +=1
                continue
            retry_count = 0
            logging.info(f"Socket connected to the adderess {HOST}:{PORT}")
            
            print("Enter data to send to the server: ", end="")
            data = input()
            
            logging.info(f"Sending data to server: {data}")
            
            try: 
                client.sendall(bytes(data,"utf-8"))       
            except TimeoutError as e:
                logging.error(f"No respose from server after {TIMEOUT} seconds")
            
            logging.info(f"Data Sent to server.")
            server_data = client.recv(1024)
            logging.info(f"Recived Data from server: {server_data!r}")
            time.sleep(1)
            logging.info("Terminating connection with server")
            break
        
        if(retry_count >= MAX_TRIES):
            logging.error(f"No response from server tried {retry_count} times")
        