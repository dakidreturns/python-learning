import socket
import logging
 
if __name__ == "__main__":
    HOST = "127.0.0.1"
    PORT = 34983
    #PORT = 34983
    MAX_TRIES = 3
    TIMEOUT = 10

    format = "[%(levelname)s] %(asctime)s : %(msg)s"
    time_format = "%H:%M:%S"
    retry_count = 0

    logging.basicConfig(format=format, datefmt=time_format, level=logging.INFO)
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server:
        while retry_count < MAX_TRIES:
            server.settimeout(TIMEOUT)
            
            logging.info(f"Binding to Address ({HOST}:{PORT})")
            server.bind((HOST,PORT))
            logging.info(f"Socket bound to the adderess {HOST}:{PORT}")
            
            try:
                server.listen()
            except TimeoutError:
                logging.error(f"Waited for {TIMEOUT} seconds no client arrived... Retrying...")
                retry_count +=1
                continue

            retry_count =0
            logging.info(f"Socket Listening for connection..... ")
            connection, addr = server.accept()
            
            with connection:
                logging.info(f"Connection recived from {addr}")
                connection.settimeout(TIMEOUT)
                while retry_count< MAX_TRIES:
                    try:
                        data = connection.recv(1024)
                    except TimeoutError as e:
                        logging.error(f"Data not recived after {TIMEOUT} seconds Retrying")
                        retry_count +=1
                        continue
                    
                    logging.debug(f"Data from client: {data.hex(sep=" ")}")
                    if not data:
                        logging.info(f"Terminating connection from with client: {addr}")
                        break
                    print(f"Data from client {data}")
                    
                    logging.debug(f"Sending to Client: {data}")
                    
                    connection.sendall(data)
                    
            break

        if retry_count >= 3:
            logging.error(f"No resopnse from client(s) tried {retry_count} times")