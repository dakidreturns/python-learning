LOGGING_FORMAT = "[%(levelname)s] %(asctime)s %(threadName)s : %(message)s"
DATETIME_FORMAT = "%H:%M:%S"
HOST = "127.0.0.1"
PORT = 32457
TIMEOUT = 10
MAX_RETRY = 3