import multiprocessing
import constants
import time
import logging
import threading 
import client
import server

def init():
    logging.basicConfig(format=constants.LOGGING_FORMAT,datefmt=constants.DATETIME_FORMAT,level=logging.INFO)

if __name__ == "__main__":
    init()
    server_thread = threading.Thread(target = server.server, name="server-thread")
    client_thread = threading.Thread(target = client.client, name="client-thread")

    logging.info("Starting server thread.")
    server_thread.start()
    logging.info("Starting client thread.")
    client_thread.start()

    server_thread.join()
    client_thread.join()