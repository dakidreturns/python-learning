import socket
import logging
import constants

def client():
    with socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM) as client_socket:
        client_socket.connect((constants.HOST,constants.PORT))
        logging.info("Connected... to server")
        print("Enter data to send to server: ", end = "")
        data = input()
        logging.debug(f"Sending Data to Server: {data}")
        client_socket.send(bytes(data,"utf-8"))
        logging.info("Data sent to server.")