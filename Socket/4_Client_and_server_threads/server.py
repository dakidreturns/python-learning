import socket
import logging
import constants

def server():
    with socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM) as server_socket:
        server_socket.bind((constants.HOST,constants.PORT))
        server_socket.listen()
        client_connection, addr = server_socket.accept()
        with client_connection:
            data = client_connection.recv(1024)
            if(not data):
                server_socket.__exit__()
            logging.info(f"Recived data from client: {data}")