import logging
import http.client
import threading
import time
import utils.helpers

from queue import Queue

from utils.helpers import get_person_info
from service.menu import Menu

'''
    Creates the person object and sends it to the queue
'''

def pipe_writer(pipeline:Queue):
    person = get_person_info()
    
    if(person == None or person.containsNone()):
        raise ValueError

    logging.info(f"Adding person {person} to pipe" )
    pipeline.put(person)


class UI_Thread(threading.Thread):
    def __init__(self, name:str, pipeline:Queue, threadDeadEvent:threading.Event):
        threading.Thread.__init__(self,name=name)
        self.pipeline = pipeline
        self.threadDeadEvent = threadDeadEvent
        
        self.menu = Menu([  "Enter User  ",
                            "Search User (not updated) ", 
                            "Auto Mode",
                            "Exit"
                        ])


    def handle_choice(self, choice):
        if(choice == 1):
            pipe_writer(self.pipeline)
        elif(choice == 2):
            raise NotImplementedError()
        elif(choice == 3 ):
            self.auto_mode(1.5)
        elif(choice == 4):
            self.threadDeadEvent.set()

    def auto_mode(self, interval = 1):
        while(True):
            time.sleep(interval)
            self.pipeline.put( utils.helpers.create_random_person())

    def run(self):
        count = 0
        while(True):
            if(self.threadDeadEvent.is_set()):
                logging.warn("Thread is dead....")
                break
            try:
                choice = self.menu.run_menu()
            except EOFError:
                self.threadDeadEvent.set()
                break
            except ValueError:
                logging.error(f"Enter integer values between 1 and {len(self.menu_choices)}")
                continue
            
            try:
                self.handle_choice(choice)
            except EOFError:
                self.threadDeadEvent.set()
            except Exception as e:
                logging.error(f"Exception occured : {e}")
            choice = -1
            
        logging.info(f"Added {count} number of persons")

if __name__ == "__main__":
    pass