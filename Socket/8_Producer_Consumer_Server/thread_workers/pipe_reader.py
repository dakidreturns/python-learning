import queue
import json
import logging
import threading

import models.person as person
import utils.constants
import utils.helpers

from utils.constants import DATABASE_FILE
from service.database_service import dbContext


class PipeReader(threading.Thread):

    def __init__(self,name:str, pipeline:queue.Queue,threadDeadEvent:threading.Event):
        threading.Thread.__init__(self,name=name)
        self.pipeline = pipeline
        self.threadDeadEvent = threadDeadEvent


    # def send_request_to_server(self, person:person.Person)->map:
    #     '''
    #         Prameters:
    #             person - the person data that needs to be send to the server
    #         Returns:
    #             The response code and the respose message in a map
    #     '''
    #     headers:map = {"Content-Type":"application/json"}
    #     body = json.dumps(person,default=utils.helpers.obj_to_dict)
    #     # response = self.serverConnection.send_request("POST",r"/api?person_data",body,headers)
    #     self.serverConnection.request("POST",r"/api?person_data",body,headers)
    #     response = self.serverConnection.getresponse()
    #     return {"status": response.status, "reason":response.reason}
        

    def run(self):
        '''
            Parameters:
                database: Database to which data is to be stored.
                pipeline: A queue which has item from where it reads data.
                event: threading.Event() to notify producer thread it took item from queue.
            Returns:
                None.
        '''
        count = 0
        while(True):
            if(self.threadDeadEvent.is_set()):
                break
            try:
                person = self.pipeline.get(timeout=3)
            except queue.Empty:
                logging.debug("No data recieved, Reader sad!")
                continue
            
            dbContext.add_entry('person_data', person)
            dbContext.save_to_database()
            # file_handler.write_object_to_file(DATABASEFILE,person)
            
            logging.info(f"Got person: {person} from pipe" )
            
            logging.debug(f"Recived {count} amount of persons")
            
            logging.debug(f"Pipe Status: {self.pipeline.not_empty}")
            count += 1
        logging.info("Reader exiting.")