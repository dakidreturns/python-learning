import threading
import logging
import http.server
import service.HTTPRequestHandler as HTTPRequestHandler

class ServerThread(threading.Thread):
    def __init__(self, name, hostName:str, portNum:int, isDaemon:bool):
        threading.Thread.__init__(self, name=name,daemon=isDaemon)
        self.hostName = hostName
        self.portNum = portNum 

    def run(self):
        logging.info(f"Starting Server at: ({self.hostName}:{self.portNum}) ")
        server = http.server.HTTPServer((self.hostName,self.portNum), HTTPRequestHandler.HTTPRequestHandler)
        try:
            server.serve_forever()
        except KeyboardInterrupt:
            pass
        server.server_close()