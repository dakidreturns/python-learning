import logging
import threading

from queue import Queue

import utils.constants 
from thread_workers.pipe_reader import PipeReader
from thread_workers.ui_thread import UI_Thread
from thread_workers.server import ServerThread

def init():
    logging.basicConfig(format=utils.constants.LOGGING_FORMAT,datefmt=utils.constants.DATETIME_FORMAT, level=logging.DEBUG)
    threadDeadEvent = threading.Event()
    pipeline = Queue()
    return (threadDeadEvent, pipeline)


if __name__ == "__main__":
    threadDeadEvent, pipeline = init()

    server_thread = ServerThread("Server-Thread",utils.constants.HOST, utils.constants.PORT, isDaemon=True)
    UI_thread     = UI_Thread ("UI-Thread"     ,pipeline,threadDeadEvent)
    reader_thread = PipeReader("Reader-Thread" ,pipeline,threadDeadEvent)

    server_thread.start()
    UI_thread.start()
    reader_thread.start()

    UI_thread.join()
    reader_thread.join()

    print("\n")
    # DisplayDataInDatabase(database)