LOGGING_FORMAT = "[%(levelname)s] %(asctime)s %(threadName)s : %(message)s"
DATETIME_FORMAT = "%H:%M:%S"
HOST = "<the required ip>"
PORT = 8080
TIMEOUT = 10
MAX_RETRY = 3
DATABASE_FILE = r"<path to Data.json>"