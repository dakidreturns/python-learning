class InvalidAgeException(Exception):
    "Raised when the input value is less than 0"
    def __str__(self):
        return "The age cannot be less than 0"
    pass