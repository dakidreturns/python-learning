import random
from faker import Faker
from models.person import Person
from utils.exception import InvalidAgeException

fake = Faker()

def create_random_person()-> Person:
    return Person(name=fake.name(), age=random.randint(1,100))

def obj_dict(obj:object):
    return obj.__dict__

def get_person_info():
    '''
    Reads data from console and retuns a person object
        Parameters: A thread event that is set when there is an error in input
    '''
    print("Enter name of person: ")
    name = input()

    print("Enter age of person: ")
    age_string = input()

    age = int(age_string)
    if age < 0:
        raise InvalidAgeException("Age cant be less than 0")

    person = Person(name=name,age=age)
    return person


def DisplayDataInDatabase(database:list):
    '''
    Print a list of data to console.
        Parameters: database -> list of object that has their __str__() function defined
    '''
    for data in database:
        print(str(data))