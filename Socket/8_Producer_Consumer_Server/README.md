# Server with a producer and consumer  

In this program, 3 threads are created, a producer, a consumer and a server. The producer creates data and sends it to the consumer.  
The server, upon request of an external application sends the data to the application.