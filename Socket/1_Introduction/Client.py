import socket
import logging
import time

if __name__ == "__main__":
    HOST = "127.0.0.1"
    PORT = 34983
    #PORT = 34983

    data = "This is a good sign"
    format = "[%(levelname)s] %(asctime)s : %(msg)s"
    time_format = "%H:%M:%S"

    logging.basicConfig(format=format, datefmt=time_format, level=logging.INFO)

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client:
        client.connect((HOST,PORT))
        logging.info(f"Socket connected to the adderess {HOST}:{PORT}")
        logging.info(f"Sending data to server: {data}")
        client.sendall(bytes(data,"utf-8"))       
        logging.info(f"Data Sent to server.")
        server_data = client.recv(1024)
        logging.info(f"Recived Data from server: {server_data!r}")
        time.sleep(1)
        logging.info("Terminating connection with server")
        