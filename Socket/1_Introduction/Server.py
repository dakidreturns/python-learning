import socket
import logging
 
if __name__ == "__main__":
    HOST = "127.0.0.1"
    PORT = 34983
    #PORT = 34983

    format = "[%(levelname)s] %(asctime)s : %(msg)s"
    time_format = "%H:%M:%S"

    logging.basicConfig(format=format, datefmt=time_format, level=logging.DEBUG)
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server:
        logging.info(f"Binding to Address ({HOST}:{PORT})")
        server.bind((HOST,PORT))
        logging.info(f"Socket bound to the adderess {HOST}:{PORT}")
        server.listen()
        logging.info(f"Socket Listening for connection..... ")
        connection, addr = server.accept()
        
        with connection:
            logging.info(f"Connection recived from {addr}")
            while True:
                data = connection.recv(1024)
                logging.debug(f"Data from client: {data.hex(sep=' ')}")
                if not data:
                    logging.info(f"Terminating connection from with client: {addr}")
                    break
                print(f"Data from client {data}")
                
                logging.debug(f"Sending to Client: {data}")
                connection.sendall(data)