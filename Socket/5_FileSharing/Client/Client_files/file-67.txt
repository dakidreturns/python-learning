Patroclus did as his dear comrade had bidden him. 
He brought Briseis from the tent and gave her over to the heralds, who took her with them to the ships of the Achaeans�and the woman was loth to go.