import socket
import logging
import threading
import client_handler
import constants


def init():
    logging.basicConfig(format=constants.LOGGING_FORMAT,datefmt=constants.DATETIME_FORMAT, level=logging.DEBUG)

if __name__ == "__main__":
    init()

    client_num = 0
    client_threads = list()
    
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server:
        server.settimeout(constants.TIMEOUT)

        logging.debug(f"Binding to Address ({constants.HOST}:{constants.PORT})")
        server.bind((constants.HOST,constants.PORT))
        logging.info(f"Socket bound to the adderess {constants.HOST}:{constants.PORT}")
        server.listen()
        logging.info(f"Socket Listening for connection..... ")
        
        while True:
            try:
                connection, addr = server.accept()
                logging.info(f"Recived client: {addr}")
            except TimeoutError:
                logging.error("Server timed out when waiting for connection... trying to exit after finishing all client operations.")
                break

            # client_thread = threading.Thread(target=client_handler.client_handler, args=(connection,addr,client_num), name=f"client-{client_num}")
            client_thread = client_handler.ClientHandler(name=f"client-{client_num}", connection=connection, address=addr)
            client_thread.start()
            client_num += 1
            client_threads.append(client_thread)

        for thread in client_threads:
            thread.join()
            logging.info(f"{thread.name} Has finished executing")
    logging.info("Server Exiting....")