import socket
import os.path
import random
import randomText
import constants
import logging

import socket_file_transfer

def create_random_file(file_name):
    with open(file_name,"w") as file:
        file.write(randomText.get_random_text())

def init():
    logging.basicConfig(format=constants.LOGGING_FORMAT,datefmt=constants.DATETIME_FORMAT, level=logging.DEBUG)

if __name__ == "__main__":
    init()
    
    file_name = f"file-{random.randint(1,100)}.txt"
    # file_name = "iliad.txt"
    full_file_name = f"{constants.CLIENT_FILE_READ_DIR}/{file_name}"
    
    logging.debug("Creating socket..... ")
    
    with socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM) as client_socket:
        client_socket.settimeout(constants.TIMEOUT)

        logging.debug("Socket Created...")
        logging.debug("Trying to connect to Server")
        
        client_socket.connect((constants.HOST,constants.PORT))
        
        logging.info("Connected to server")

        if(not os.path.exists(full_file_name)):
            logging.info(f"Cant find the required file: {file_name} creating a random file...")
            create_random_file(full_file_name)

        logging.info(f"Sending file name: {file_name}")
        
        # Send file name
        socket_file_transfer.send_file_name(client_socket,file_name)

        # Get acknowledgement from server
        acknowlegement = socket_file_transfer.get_acknowledgement(client_socket)

        #Send file size
        socket_file_transfer.send_file_size(client_socket,full_file_name)
        
        acknowlegement = socket_file_transfer.get_acknowledgement(client_socket)
        logging.debug(f"Server says: {acknowlegement}")

        #Send the file
        socket_file_transfer.send_file_data(client_socket,full_file_name)

        acknowlegement = socket_file_transfer.get_acknowledgement(client_socket)
        logging.info(acknowlegement)

        logging.debug("Waiting for file from server....")
        file_name = socket_file_transfer. get_file_name(client_socket)
        logging.info(f"Server will send the file : {file_name}")
        socket_file_transfer.send_acknowledgement(client_socket,"Recived file name")
        
        file_size = socket_file_transfer.get_file_size(client_socket)
        logging.info(f"File {file_name} has size {file_size} bytes")

        socket_file_transfer.send_acknowledgement(client_socket,"Recived file size")
        
        logging.info(f"Saving file {file_name} ")
        socket_file_transfer.write_to_file(client_socket,
                                           f"{constants.CLIENT_FILE_SAVE_DIR}/{file_name}",
                                           file_size,
                                           1024)
        logging.info(f"File Saved to {constants.CLIENT_FILE_SAVE_DIR}/{file_name}")