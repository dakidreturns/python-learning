import logging
import socket
import os

def send_file_data(connection:socket.socket,file_name:str ):
    with open(file_name, 'rb') as file_to_send:      
        while(content := file_to_send.read(1024)):
            logging.debug(f"Sending data {content}")
            connection.send(content)

def send_file_name(connection:socket.socket,file_name:str,):
    connection.send(bytes(file_name,'utf-8'))

def send_file_size(connection:socket.socket, file_name:str):
    file_size = os.path.getsize(file_name)
    logging.debug(f"File size = {file_size}")
    file_size_in_bytes = bytes(str(file_size),'utf-8')
    logging.debug(f"File size in bytes = {file_size_in_bytes}")
    connection.send(file_size_in_bytes)

def get_acknowledgement(connection:socket.socket)->str:
    data_in_bytes = connection.recv(1024)
    return data_in_bytes.decode('utf-8')

def get_file_name(connection:socket.socket)->str:
    file_name =  connection.recv(1024)
    return file_name.decode('utf-8')

def get_file_size(connection:socket.socket)->int:
    file_size_bytes = connection.recv(1024)
    file_size = int(file_size_bytes.decode('utf-8'), base=10)
    return file_size

def send_acknowledgement(connection: socket.socket, message:str) -> int:
    connection.send(bytes(message,'utf-8'))

def write_to_file(connection:socket.socket, file_path:str, file_size:int,block_size:int = 1024)->int:
    write_bytes = 0
    with open(file_path,'wb') as f:
        while write_bytes < file_size:
            logging.debug("Waiting for data from client...")
            
            data = connection.recv(block_size)

            write_bytes = write_bytes+ len(data)
            
            logging.debug(f"Data Recived from client: {data}")
            logging.debug("Writing data to file")    
            f.write(data)
            
            logging.debug(f"Wrote {write_bytes} to file")
    return write_bytes