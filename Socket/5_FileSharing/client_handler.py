import socket
import logging
import constants
import time
import os
import threading
import random

import socket_file_transfer


class ClientHandler(threading.Thread):
    def __init__(self, name, connection:socket.socket, address:tuple):
        threading.Thread.__init__(self,name= name)
        self.name = name
        self.connection = connection
        self.address = address

    def run(self):
        with self.connection:
            time.sleep(random.random()*10) #simulate a delay

            file_name = socket_file_transfer.get_file_name(self.connection)
            socket_file_transfer.send_acknowledgement(self.connection,"Recived File name")
            logging.debug(f"File Name from client {file_name}")
            print(file_name)
            
            file_size = socket_file_transfer.get_file_size(self.connection)
            socket_file_transfer.send_acknowledgement(self.connection,"Recived File Size")
            logging.debug(f"File Size : {file_size}")

            logging.info(f"Reciveing file {file_name}")
            
            socket_file_transfer.write_to_file(self.connection,constants.SERVER_FILE_SAVE_DIR+'/'+file_name, file_size, block_size=1024)

            logging.info("File saved")
            socket_file_transfer.send_acknowledgement(self.connection,"Recived File Data")

            file_name = self.name
            full_file_name = constants.SERVER_FILE_READ_DIR+'/'+file_name
            if not os.path.exists(full_file_name):
                with open (full_file_name,'w') as file:
                    file.write(f"Hello {self.name} \nThere is information in this file.")
            
            socket_file_transfer.send_file_name(self.connection,file_name)

            socket_file_transfer.get_acknowledgement(self.connection)

            socket_file_transfer.send_file_size(self.connection,full_file_name)

            socket_file_transfer.get_acknowledgement(self.connection)
            
            socket_file_transfer.send_file_data(self.connection,full_file_name)

def client_handler(connection:socket.socket, address:tuple, client_num: int):
    with connection:
        time.sleep(5) #simulate a delay

        file_name = socket_file_transfer.get_file_name(connection)
        socket_file_transfer.send_acknowledgement(connection,"Recived File name")
        logging.debug(f"File Name from client {file_name}")
        print(file_name)
        
        file_size = socket_file_transfer.get_file_size(connection)
        socket_file_transfer.send_acknowledgement(connection,"Recived File Size")
        logging.debug(f"File Size : {file_size}")

        logging.info(f"Reciveing file {file_name}")
        
        socket_file_transfer.write_to_file(connection,constants.SERVER_FILE_SAVE_DIR+'/'+file_name, file_size, block_size=1024)

        logging.info("File saved")
        socket_file_transfer.send_acknowledgement(connection,"Recived File Data")

        file_name = f"Client-{client_num}"
        full_file_name = constants.SERVER_FILE_READ_DIR+'/'+file_name
        if not os.path.exists(full_file_name):
            with open (full_file_name,'w') as file:
                file.write(f"Hello client-{client_num} \nThere is information in this file.")
        
        socket_file_transfer.send_file_name(connection,file_name)

        socket_file_transfer.get_acknowledgement(connection)

        socket_file_transfer.send_file_size(connection,full_file_name)

        socket_file_transfer.get_acknowledgement(connection)
        
        socket_file_transfer.send_file_data(connection,full_file_name)