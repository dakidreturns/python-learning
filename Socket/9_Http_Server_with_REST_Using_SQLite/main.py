import utils.constants as constants
from service.serial_port_service import SerialPortStream
import logging
import serial
import service.HTTPRequestHandler as HTTPRequestHandler
import http.server 

import sys 

def init():
    ser = serial.Serial(port='/dev/ttyS4',baudrate=115200, bytesize=8, parity=serial.PARITY_NONE,stopbits=1, timeout=None)
    # ser.open()
    serial_stream = SerialPortStream(ser)

    logging.basicConfig(format=constants.LOGGING_FORMAT,datefmt=constants.DATETIME_FORMAT, level=constants.LOG_LEVEL, stream=serial_stream)
    
if __name__ == "__main__":
    init()
    
    logging.info(f"Starting Server at: ({constants.HOST}:{constants.PORT}) ")
    server = http.server.HTTPServer((constants.HOST,constants.PORT), HTTPRequestHandler.HTTPRequestHandler)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()