 
LOGGING_FORMAT = "[%(levelname)s] %(asctime)s %(threadName)s : %(message)s"

'''
Logging level in python
    NOTSET=0.
    DEBUG=10.
    INFO=20.
    WARN=30.
    ERROR=40.
    CRITICAL=50.
'''
LOG_LEVEL =30
DATETIME_FORMAT = "%H:%M:%S"

HOST = "<the required ip>"
PORT = 8080
TIMEOUT = 10
MAX_RETRY = 3

'''
Fully qualified path to the database file.
'''
DATABASE_FILE = r"<path to Data.json>"

'''
The names of tables in the database.
'''
TABLES_IN_DATABASE = ["table_1", "table_2"]

'''
SQLite scripts for creating the tables.
Scripts for each table must be in a ``[key:value]`` pair form
'''
CREATE_TABLE_SCRIPT ={
    'table_1':'''
            CREATE TABLE table_1(
                      column1 INTEGER AUTOINCREMENT,
                      column2 VARCHAR(20),
                      column3 DATETIME
            )
                      ''',}