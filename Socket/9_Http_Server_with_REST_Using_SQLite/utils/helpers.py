from utils.exception import PrimaryKeyNotFound

def obj_dict(obj:object):
    return obj.__dict__

def list_to_list_of_tuples(list_of_items:list):
    return list(map(lambda x: (x,), list_of_items))

def list_of_tuples_to_list(list_of_tuples):
    return list(map(lambda x: x[0], list_of_tuples))


# TODO: change values to MARCOS
def generate_table_creation_script(table_metadata:dict):
    table_creation_script_dict = {}
    for key, metadata in table_metadata.items():
        '''
        here key is table name and value contains the metadata for the
        table
        '''
        primary_key = metadata.get('primary_key')
        if primary_key is None:
            raise PrimaryKeyNotFound(key)
        elif type(primary_key) is not str:
            raise TypeError("Primary key must be of type str")

        table_creation_script = f"CREATE TABLE  {key} ("
        for column_name, properties  in metadata['columns'].items() :
            
            #default type to INTEGER if no type is given
            if properties.get('type') is None:
                properties['type'] = 'INTEGER'

            table_creation_script += f" {column_name} {properties['type']} "
            
            #set the primary key
            if column_name == primary_key:
                table_creation_script += " PRIMARY KEY "
            
            #set auto increment value if it exists and tru
            if properties.get('auto_increment') is not None and properties.get('auto_increment') == True:
                table_creation_script += " AUTOINCREMENT "
            
            table_creation_script += ','
        
        # Remove the trailing comma.
        table_creation_script = table_creation_script[:-1]
        table_creation_script += ')'
        table_creation_script_dict[key] = table_creation_script
    
    return table_creation_script_dict