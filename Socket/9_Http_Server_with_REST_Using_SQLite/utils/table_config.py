COLUMNS = 'columns'
TYPE = 'type'
AUTOINCREMENT = 'auto_increment'
PRIMARY_KEY = 'primary_key'

TABLE_METADATA = {
    'person_data':{ 
        COLUMNS:{
            'id' : {
                TYPE: 'INTEGER',
                AUTOINCREMENT:True
            },
            'name' : {
                TYPE: 'VARCHAR(20)',
            },
            'age' : {
                TYPE : 'INTEGER'
            }
        },
        PRIMARY_KEY: 'id'
    }
}