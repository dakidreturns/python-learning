import threading
import mmap
from collections.abc import Generator


class FileHandler(object):

    def __init__(self, filename:str):
        self.__mutex = threading.Lock()
        self._can_read = threading.Condition(self.__mutex)
        self.__filename = filename
        
    def write_data_to_file(self, data:any):
        '''
            Parameter:
                data: data to write to file
            Returns: 
                None
        '''
        with self.__mutex:
            with open(self.__filename,'w') as file:
                file.write(data)
                self._can_read.notify()
    
    def read_all_data(self)->str:
        with self.__mutex:
            with open(self.__filename,'r') as file:
                return file.read()

    def search_for_text(self, filename, text_to_search) -> Generator[str] :
        '''
        Parameters:
            ``filename``: Name of file where the text to be searched.
            ``text_to_serach``: 
        Returns:
            List containing all the lines of matching text.
        '''
        with self._can_read:
            lines =[]
            with open(filename, 'r') as file:
                for line in file.readlines():
                    if(line.find('"'+text_to_search+'"')!=-1):
                        yield line
            # return lines


    def search_for_text_mmap(self,filename,text_to_serch):
        '''
        Parameters:
            ``filename``: Name of file where the text to be searched.
            text_to_serach: 
        '''
        with self._can_read:
            with open(filename, 'rb') as file:
                with  mmap.mmap(file.fileno(),length=0,access=mmap.ACCESS_READ, offset=0) as mm_file:
                    position = mm_file.find(bytes(text_to_serch,"utf-8"))
                    mm_file.seek(position-10)
                    print(str(mm_file.readline(),'utf-8'))


if __name__ == "__main__":
    from utils.constants import DATABASE_FILE

    fileHandler = FileHandler(DATABASE_FILE)
    print(fileHandler.__dict__)
    items = fileHandler.search_for_text(DATABASE_FILE, "Rohit")
    fileHandler2 = FileHandler(DATABASE_FILE)
    print(fileHandler2.__dict__)
    print(items)