import http.server
import logging
import json
import models.person as person
import service.database_service as database_service
import re
import utils.helpers
from interface.request_handeler_interface import RequestHandlerInterface

class HTTPRequestHandler(http.server.BaseHTTPRequestHandler):

    def request_forbidden(self):
        '''
            Sends a 403 response to client when the client
            sends request to invalid url
        '''
        self.send_error(403)
        self.end_headers()


    def error_occured(self, msg:str):
        '''
            Send generic error to client incase of failure. 
            Doesn't log the error sepeatly in the server side.
        '''
        self.send_error(500,msg)
        self.end_headers()


    def resouce_not_found(self,):
        self.send_error(404)
        self.end_headers()


    def bad_request(self):
        self.send_error(400)
        self.end_headers()


    def send_Json_header(self, response_code):
        '''
            Send Content-Type : application/json
            header to client
        '''
        self.send_response(response_code)
        self.send_header('Content-Type','application/json')
        self.end_headers()


    def write_response_body(self, data:bytes):
        self.wfile.write(data)


    def send_data(self,data):
        if(data is None):
            self.resouce_not_found()
            return
        data_json = json.dumps(data, default=utils.helpers.obj_dict)
        self.send_Json_header(200)
        self.write_response_body(data_json.encode('utf-8'))


    def get_json_data_from_request(self)->dict:
        '''
            When we a are expecting a person data from client
            run this function and get the person object
            
            Returns:
                ``data_object``: a dict object containing the key value pairs of the incoming data  
        '''
        data_length = int(self.headers.get('Content-Length'))
        logging.debug(f"Client sends data of length:{data_length}")
        if(data_length == 0):
            return None
        data = self.rfile.read(data_length).decode('utf-8')
        data_object = dict(**json.loads(data))
        return data_object


    def do_GET(self):
        logging.debug(f"GET Request recived: {self.path}")
        regex_match = re.match(r"^/(?P<query>person_data)(/|\?id=(?P<id>[0-9]+)/?)?$",self.path)

        logging.debug(f"{regex_match}")
        
        if(regex_match is None):
            self.request_forbidden()
            return
        
        regex_match_dict = regex_match.groupdict()
        print(regex_match_dict)
        data = RequestHandlerInterface.get_data(regex_match_dict)
        self.send_data(data)
        
        
    def do_POST(self):
        regex_match = re.match(r"^/(?P<query>person_data)/?$",self.path)
        print(f"Regex amtch from post: {regex_match}")

        if(regex_match is None):
            self.request_forbidden()
            return 
        
        request_dict = regex_match.groupdict()
        try:
            data_object = self.get_json_data_from_request()
        except json.decoder.JSONDecodeError:
            self.bad_request()
            return
        
        logging.debug(f"{data_object}")

        try:
            response = RequestHandlerInterface.add_item(request_dict=request_dict,json_data=data_object)
            if response is not None:
                self.send_response(201)
            else:
                self.error_occured("Error occured during addition")
        except Exception as e: 
            self.error_occured("Error occured during addition")
            logging.error(e.__str__())
        self.end_headers()

    
    def do_PUT(self):

        regex_match = re.match(r"^/(?P<query>person_data)\?id=(?P<id>[0-9]+)/?$",self.path)
        
        logging.debug(f"{regex_match}")
        if(regex_match is None ):
            self.request_forbidden()
            return
        logging.debug(f"{regex_match.groups()}")
        
        id = int(regex_match['id'])
        data_set = regex_match['query']
        
        data_object = self.get_person_data_from_request()
        
        if data_object is None or data_object.containsNone():
            self.bad_request()
            return 
        
        updated_item = database_service.dbContext.update_item(data_set,id,data_object)
        
        if(updated_item is None):
            self.resouce_not_found()
            return
        
        database_service.dbContext.save_to_database()
        self.send_Json_header(201)
        self.write_response_body(json.dumps(updated_item, default=utils.helpers.obj_dict).encode('utf-8'))
        self.end_headers() 


    def do_DELETE(self):
        regex_match = re.match(r"^/(?P<query>person_data)\?id=(?P<id>[0-9]+)/?$",self.path)
        
        if(regex_match is None or regex_match.groupdict().get('id') is None):
            self.request_forbidden()
            self.send_error(403)
            return
        
        logging.debug(f"{regex_match.groups()}")

        id = int(regex_match['id'])
        data_set = regex_match['query']
        item = database_service.dbContext.delete_item(data_set,id)
        if(item is None):
            self.resouce_not_found()
            return
        database_service.dbContext.save_to_database()
        self.send_response(200)
        self.end_headers()



    def log_message(self, format, *args):
        """Log an arbitrary message.

        This is used by all other logging functions.  Override
        it if you have specific logging wishes.

        The first argument, FORMAT, is a format string for the
        message to be logged.  If the format string contains
        any % escapes requiring parameters, they should be
        specified as subsequent arguments (it's just like
        printf!).

        The client ip and current date/time are prefixed to
        every message.

        Unicode control characters are replaced with escaped hex
        before writing the output to stderr.

        """

        message = format % args
        logging.info(f"{self.address_string()} - - {message}" )