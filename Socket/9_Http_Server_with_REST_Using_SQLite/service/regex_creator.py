# import utils.constants as constants
from http import HTTPMethod

data_types = {"numeric" : "[0-9]+",
              "alphanumeric":"[0-9a-zA-Z]+"
              }

class UrlRegexCreator:
    @staticmethod
    def create_parameter_string(parameters_with_data_type):     
        parameter_string = "|".join([f"\\?{parameter}=(?P<{parameter}>{data_types[datatype]})" for parameter,datatype in parameters_with_data_type.items()])
        return parameter_string
    
    @staticmethod
    def get_regex(method:HTTPMethod,queries, parameters):
        queries_string = '|'.join(queries)
        if(parameters != None):
            parameters_string = UrlRegexCreator.create_parameter_string(parameters)
        
        if(method == HTTPMethod.GET):
            regex_string = f"^/(?P<query>{queries_string})(/|{parameters_string}/?)?$"
        elif(method == HTTPMethod.POST):
            regex_string = f"^/(?P<query>{queries_string})/?$"
        elif(method == HTTPMethod.PUT or method == HTTPMethod.DELETE):
            regex_string = f"^/(?P<query>{queries_string})({parameters_string})/?$"
        return regex_string
    
if __name__ == "__main__":
    print(UrlRegexCreator.get_regex(HTTPMethod.GET,['person_data','sample_data' ],{'id':'numeric','uid': 'alphanumeric'}))
    