import models.person
import utils.constants
import utils.helpers
from utils.table_config import TABLE_METADATA, AUTOINCREMENT,COLUMNS
from utils.exception import TableCreationScriptNotFound
from utils.helpers import generate_table_creation_script

import sqlite3
import logging

class Database:
    def __init__(self, dbfile):
        '''
            Create database connection with the given file name as dbfile
        '''
        self.connection = sqlite3.connect(dbfile)

        # get all tables in database
        list_of_tables = self.__get_all_tables_in_database()

        # get tables to add
        tables_to_add = self.__get_tables_to_add(list_of_tables)
        
        # add the tables.
        if len(tables_to_add) > 0:
            tables_created = self.__add_tables(tables_to_add)


    def __get_all_tables_in_database(self)->list[str]:
        cur = self.connection.cursor()
        res = cur.execute("SELECT name FROM sqlite_master WHERE type='table' ")
        tables_in_database = res.fetchall()
        cur.close()
        list_of_tables = utils.helpers.list_of_tuples_to_list(tables_in_database)
        return list_of_tables

    def __get_tables_to_add(self,list_of_tables)->list[str]:
        tables_to_add = []
        for table in utils.constants.TABLES_IN_DATABASE:
            if table not in list_of_tables:
                tables_to_add.append(table)
        return tables_to_add

    def __add_tables(self, list_of_tables_to_add)->list[str]:
        '''
            Returns:
                The names of tables added.
        '''
        tables_created = []
        cur = self.connection.cursor()
        table_creation_script_dict = generate_table_creation_script(TABLE_METADATA) 
        
        for table in list_of_tables_to_add:
            table_creation_script = table_creation_script_dict.get(table)
            if(table_creation_script is None):
                raise TableCreationScriptNotFound
            cur.execute(table_creation_script)
            tables_created.append(table)

        return tables_created

    def __get_column_names(self, table_name)->list[str]:
        '''
        Get all the column names from a given table. This function is meant to be
        a private member  

        Parameters:
            table_name : the table whose columns needs to be fetched.
        Returns:
            list of strings containing all the column name.
        '''

        cursor = self.connection.cursor()
        cursor.execute(f"PRAGMA table_info({table_name})")
        columns = cursor.fetchall()

        column_names = [x[1] for x in columns]
        cursor.close()
        return column_names
    
    def __create_parameter_string(self,values:list):
        '''
            Function for creating parameter string the of the form 
            `(?,?,?)` the number of ? corresponds to the number of 
            values to be inserted.
        '''
        parameter_string = '('
        for _ in  values:
            parameter_string += '?,'
        parameter_string = parameter_string[:-1]
        parameter_string += ')'
        return parameter_string
    
    def __perpare_data_for_addition(self, table_name:str, data_dict:dict):
        column_names = self.__get_column_names(table_name)
        values = []
        for column in column_names:
            value = data_dict.get(column,None)
            column_meta_data = TABLE_METADATA[table_name][COLUMNS].get(column)
            #logging.debug(f"{column} = {column_meta_data}")
            if column_meta_data.get(AUTOINCREMENT) is not None and column_meta_data[AUTOINCREMENT] == True:
                value = None
            values.append(value)
        return values



    def table_exits_in_database(self, table_name):
        table_names = self.__get_all_tables_in_database()
        return table_name in table_names

    def save_to_database(self):
        '''
            Write data to the PersonDataList and then commit the list to the json file
            using file handler.
        '''
        self.connection.commit()
    


    def add_entry(self, table_name,data_dict):
        '''
            Add a single person to database, does not commit to the database.
            ``person``: a Person object that needs to be added to the database.
        '''  
        if not self.table_exits_in_database(table_name):
            return None
        
        cursor = self.connection.cursor()
        # data_dict = vars(data)
        
        value_list = self.__perpare_data_for_addition(table_name,data_dict)
        parameter_string = self.__create_parameter_string(value_list)
        
        print(f"INSERT INTO {table_name} VALUES {parameter_string}")
        cursor.execute(f"INSERT INTO {table_name} VALUES {parameter_string}",value_list)
        cursor.close()
        return data_dict
    

    def find_item(self,data_set,id:int) -> dict:
        '''
            Provide id and get the item
            Parameters: 
                ``id``: ``id`` of the person to be found
            Returns:
                A person object if the id is in the database, else returns None
        '''
        if not self.table_exits_in_database(data_set):
            return None
        
        column_names = self.__get_column_names(data_set)
        cursor = self.connection.cursor()
        res = cursor.execute(f"SELECT * FROM {data_set} WHERE id = {id}")
        
        data = res.fetchone()
        logging.debug(f"Item found {data}")
        if(data is None ):
            return None
        data_dict = {x:y for x,y in zip(column_names,data)}
        cursor.close()
        return data_dict
    
    def update_item(self,data_set:str, id:int, updated_data) -> models.person.Person:
        '''
            Update a single item from the database.
            Parameters:
                ``id``: The ``id`` of the person to be updated.
            Returns:
                The updated Person object if the ``id`` is found else ``None``
        '''
        if not self.table_exits_in_database(data_set):
            return None
        column_names = self.__get_column_names(data_set)
        cursor = self.connection.cursor()
        cursor.execute(f"SELECT * FROM {data_set} WHERE id = {id}")
        data = cursor.fetchone()
        if(data is None):
            return None

        data_dict = {x:y for x,y in zip(column_names,data)}
        updated_data_dict:dict = vars(updated_data)
        
        
        for key in updated_data_dict.keys():
            if(key == 'id'):
                continue
            data_dict[key] = updated_data_dict[key]
        
        data_dict.pop('id')
        
        parameter_string = ", ".join([f"{key} = '{value}'"   for key, value in data_dict.items()])
        cursor.execute(f"UPDATE {data_set} SET {parameter_string} WHERE id = {id}")
        cursor.close()
        return data_dict
    
    def delete_item(self, data_set,id:int) -> dict:
        '''
        Delete an item corresponding to the given id.
        Internally calles ``find_item`` function to get the data.
        Parameters:
            ``data_set`` : name of the table where the data is
            ``id``       : id of the data to be deleted.
        Returns:
            A dictionary containing the deleted data.
        '''

        if not self.table_exits_in_database(data_set):
            return None
        data = self.find_item(data_set,id)
        
        if(data is None):
            return None
        
        cursor = self.connection.cursor()
        cursor.execute(f"DELETE from {data_set} WHERE id = {id}")

        return data


    def get_all_data(self, data_set:str) -> list[dict] :
        '''
        Returns all the data in the table.
        Parameters:
            `data_set` : The table from which the data needs to be fetched.
        Returns: 
            A list of dict containing all the data
        '''

        if not self.table_exits_in_database(data_set):
            return None
        column_names = self.__get_column_names(data_set)
        cursor = self.connection.cursor()
        res = cursor.execute(f"SELECT * FROM {data_set}")
        data = [ {z:y for z,y in zip(column_names,x)} for x in res.fetchall() ]
        
        return data

'''
A single object through which data is accessed.
So multiple database objects will not be created for 
multiple requests.
'''
dbContext = Database(utils.constants.DATABASE_FILE)