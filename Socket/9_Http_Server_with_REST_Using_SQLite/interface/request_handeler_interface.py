from service.database_service import dbContext
from utils.table_config import TABLE_METADATA


QUERY ='query'
PRIMARY_KEY = 'primary_key'

class RequestHandlerInterface:
    @staticmethod
    def __clean_request_dict(request_dict:dict)->str:
        request_dict_copy = request_dict.copy()
        for key,value in request_dict_copy.items():
            if(value is None):
                request_dict.pop(key)
        return request_dict
    
    @staticmethod
    def get_data(request_dict:dict):
        '''
            Get the data from the database and send it to the HTTPRequestHandler.
        '''
        request_dict = RequestHandlerInterface.__clean_request_dict(request_dict)

        table_name =  request_dict.get(QUERY)
        if table_name is None: 
            return None
        '''
            If there is only one key in the dictionary it is 
        '''
        if(len(request_dict.keys()) == 1):
            return {table_name: dbContext.get_all_data(table_name)}

        primary_key = TABLE_METADATA.get(table_name)[PRIMARY_KEY]
        if(primary_key is None):
            return None
        
        primary_key_value = request_dict.get(primary_key)
        if(primary_key_value is None ):
            return None
        
        return dbContext.find_item(table_name,primary_key_value)
    
    @staticmethod
    def add_item(request_dict:dict, json_data:dict):
        
        table_name =  request_dict.get(QUERY)
        if table_name is None: 
            return None
        
        try:
            data = dbContext.add_entry(table_name,json_data)
            dbContext.save_to_database()
        except Exception as e:
            raise e
        return data