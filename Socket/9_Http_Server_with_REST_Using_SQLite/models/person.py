class Person:
    def __init__(
            self,
            id:int=None, 
            name:str=None, 
            age:int = None, 
            *args, 
            **kwargs
            ):
        self.id = id
        self.name = name
        self.age = age
        
    def __str__(self):
        return f"{self.id} {self.name} {self.age}"
    def __json__(self):
        return self.__dict__
    def containsNone(self):
        return self.name is None or self.age is None