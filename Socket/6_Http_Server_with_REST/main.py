import utils.constants as constants
import logging

import service.HTTPRequestHandler as HTTPRequestHandler
import http.server

import sys 

def init():
    logging.basicConfig(format=constants.LOGGING_FORMAT,datefmt=constants.DATETIME_FORMAT, level=constants.LOG_LEVEL)
    
if __name__ == "__main__":
    init()
    
    logging.info(f"Starting Server at: ({constants.HOST}:{constants.PORT}) ")
    server = http.server.HTTPServer((constants.HOST,constants.PORT), HTTPRequestHandler.HTTPRequestHandler)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()