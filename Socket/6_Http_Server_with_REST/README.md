# Basic REST Server with python

A basic server that contains a database, a REST API to do GET, POST, PUSH and DELETE on a database. 


## How to run.  
#### Step 1: Configure `constants.py`  

This file contains basic configurations for path of database, the host ip address etc.  
Create a copy of [`constants_example.py`](./utils/constants_example.py) and name it as `constants.py`. Modify the `HOST`, `DATABASE_FILE`, `TABLES_IN_DATABASE` and `CREATE_TABLE_SCRIPT` as required.  

Example:
```python
HOST = "127.0.0.1" # Hosting on local host
DATABASE_FILE = 'C:/User/<Username>/Documents/example.db' # A fully qualified name is prefered
```

Example for `TABLES_IN_DATABASE` and `CREATE_TABLE_SCRIPT` are in [`constants_example.py`](./utils/constants_example.py)

#### Step 2: Create the corresponding models in models folder



## Appendix: Tips
1. To print the logging info in database service during creation of `dbContext`. Move the `init()` function in main to just above the import statement of  `HTTPRequestHandler`