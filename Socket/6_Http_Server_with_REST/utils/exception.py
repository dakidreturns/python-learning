class TableCreationScriptNotFound(Exception):
    def __init__(self, table_name:str = None):
        Exception.__init__(self)
        self.table_name = table_name
    def __str__(self):
        return "The script for table creation is not found" if self.table_name is None else f"The script for creating {self.table_name} is not found"