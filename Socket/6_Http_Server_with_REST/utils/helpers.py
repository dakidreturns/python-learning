def obj_dict(obj:object):
    return obj.__dict__

def list_to_list_of_tuples(list_of_items:list):
    return list(map(lambda x: (x,), list_of_items))

def list_of_tuples_to_list(list_of_tuples):
    return list(map(lambda x: x[0], list_of_tuples))