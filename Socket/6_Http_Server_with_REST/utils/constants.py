LOGGING_FORMAT = "[%(levelname)s] %(asctime)s %(threadName)s : %(message)s"
LOG_LEVEL = 10
DATETIME_FORMAT = "%H:%M:%S"
# HOST = "192.168.2.116"
HOST = ""
PORT = 2389
TIMEOUT = 10
MAX_RETRY = 3
DATABASE_FILE = r"C:/Users/Rohith Karunakaran/Training/pythontest/Technical/Sources/Software/Python-Rohit/Socket/6_Http_Server_with_REST/Data/Data.db"
TABLES_IN_DATABASE = ['person_data' ]
CREATE_TABLE_SCRIPT ={
    'person_data':'''
            CREATE TABLE person_data(
                      id INTEGER PRIMARY KEY AUTOINCREMENT,
                      name VARCHAR(20),
                      age INTEGER
            )''',}