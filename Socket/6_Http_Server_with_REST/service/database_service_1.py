import models.person
import utils.constants
import utils.helpers
from utils.exception import TableCreationScriptNotFound

import sqlite3
import logging

class Database:
    def __init__(self, dbfile):
        '''
            Create database connection with the given file name as dbfile
        '''
        self.connection = sqlite3.connect(dbfile)

        # get all tables in database
        list_of_tables = self.__get_all_tables_in_database()

        # logging.debug(f"List of tables : {list_of_tables}")
        # logging.debug(f"Tables required : {utils.constants.TABLES_IN_DATABASE}")

        # get tables to add
        tables_to_add = self.__get_tables_to_add(list_of_tables)

        # logging.debug(f"Tables to add : {tables_to_add}")
        
        # add the tables.
        if len(tables_to_add) > 0:
            tables_created = self.__add_tables(tables_to_add)
            # logging.debug(f"Tables created : {tables_created}")


    def __get_all_tables_in_database(self)->list[str]:
        cur = self.connection.cursor()
        res = cur.execute("SELECT name FROM sqlite_master WHERE type=table")
        tables_in_database = res.fetchall()
        cur.close()
        list_of_tables = utils.helpers.list_of_tuples_to_list(tables_in_database)
        return list_of_tables

    def __get_tables_to_add(self,list_of_tables)->list[str]:
        tables_to_add = []
        for table in utils.constants.TABLES_IN_DATABASE:
            if table not in list_of_tables:
                tables_to_add.append(table)
        return tables_to_add

    def __add_tables(self, list_of_tables_to_add)->list[str]:
        '''
            Returns:
                The names of tables added.
        '''
        tables_created = []
        cur = self.connection.cursor()
        for table in list_of_tables_to_add:
            table_creation_script = utils.constants.CREATE_TABLE_SCRIPT.get(table)
            if(table_creation_script is None):
                raise TableCreationScriptNotFound
            cur.execute(table_creation_script)
            tables_created.append(table)

        return tables_created

    def __get_column_names(self, table_name)->list[str]:
        cursor = self.connection.cursor()
        cursor.execute(f"PRAGMA table_info({table_name})")
        column_names = [x[1] for x in cursor.fetchall()]
        cursor.close()
        return column_names

    def table_exits_in_database(self, table_name):
        table_names = self.__get_all_tables_in_database()
        return table_name in table_names

    def save_to_database(self):
        '''
            Write data to the PersonDataList and then commit the list to the json file
            using file handler.
        '''
        self.connection.commit()
        pass

    def add_entry(self, data_set,data):
        '''
            Add a single person to database, does not commit to the database.
            ``person``: a Person object that needs to be added to the database.
        '''  
        if not self.table_exits_in_database(data_set):
            return None
        cursor = self.connection.cursor()
        data_dict = vars(data)
        
        parameter_string = '('
        value_list = []
        for key,value in  data_dict.items():
            parameter_string += '?,'
            value_list.append(value)
        parameter_string = parameter_string[:-1]
        parameter_string += ')'

        cursor.execute(f"INSERT INTO {data_set} VALUES {parameter_string}",value_list)
        cursor.close()
    

    def find_item(self,data_set,id:int) -> models.person.Person :
        '''
            Provide id and get the item
            Parameters: 
                ``id``: ``id`` of the person to be found
            Returns:
                A person object if the id is in the database, else returns None
        '''
        if not self.table_exits_in_database(data_set):
            return None
        
        column_names = self.__get_column_names(data_set)
        cursor = self.connection.cursor()
        res = cursor.execute(f"SELECT * FROM {data_set} WHERE id = {id}")
        
        data = {x:y for x,y in zip(column_names,res.fetchone())}
        cursor.close()
        return data
    
    def update_item(self,data_set:str, id:int, updated_data) -> models.person.Person:
        '''
            Update a single item from the database.
            Parameters:
                ``id``: The ``id`` of the person to be updated.
            Returns:
                The updated Person object if the ``id`` is found else ``None``
        '''
        
        pass
    
    def delete_item(self, data_set,id:int) -> models.person.Person :
        pass

    def get_all_data(self, data_set:str) -> list :
        if not self.table_exits_in_database(data_set):
            return None
        column_names = self.__get_column_names(data_set)
        cursor = self.connection.cursor()
        res = cursor.execute(f"SELECT * FROM {data_set}")
        data = [ {z:y for z,y in zip(column_names,x)} for x in res.fetchall() ]
        
        return data

'''
A single object through which data is accessed.
So multiple database objects will not be created for 
multiple requests.
'''
dbContext = Database(utils.constants.DATABASE_FILE)