import utils.constants
import json
import threading
import models.person
import service.file_handler_service as file_handler_service
import utils.helpers

class Database:
    def __init__(self, dbfile):
        self.database_map = {'person_data':[]}
        self.id = -1
        self._mutex = threading.Lock()

        self.file_handler = file_handler_service.FileHandler(dbfile)
        self.__load_data_from_db()

    def save_to_database(self):
        '''
            Write data to the PersonDataList and then commit the list to the json file
            using file handler.
        '''
        with self._mutex:
            json_string = json.dumps(self.database_map,default=utils.helpers.obj_dict)
            self.file_handler.write_data_to_file(json_string)
        pass

    def add_entry(self, data_set,person:models.person.Person):
        '''
            Add a single person to database, does not commit to the database.
            ``person``: a Person object that needs to be added to the database.
        '''
        if(person.id == None or person.id == -1):
            self.id = self.id+1
            person.id = self.id
            
        self.database_map[data_set].append(person)    
    
    def __load_data_from_db(self):
        '''
            Private function to load data to the dataset initially
        '''
        data = self.file_handler.read_all_data()
        if not data:
            return
        json_raw_data = json.loads(data)
        person_data_list = json_raw_data['person_data']

        for item in person_data_list:
            person_item = models.person.Person(**item)
            if person_item.id > self.id:
                self.id = person_item.id
            self.database_map['person_data'].append(person_item)

    def find_item(self,data_set,id:int) -> models.person.Person :
        '''
            Provide id and get the item
            Parameters: 
                ``id``: ``id`` of the person to be found
            Returns:
                A person object if the id is in the database, else returns None
        '''
        with self._mutex:
            for item in self.database_map[data_set]:
                if(item.id == id):
                    return item
        return None
    
    def update_item(self,data_set:str, id:int, updated_person) -> models.person.Person:
        '''
            Update a single item from the database.
            Parameters:
                ``id``: The ``id`` of the person to be updated.
            Returns:
                The updated Person object if the ``id`` is found else ``None``
        '''
        with self._mutex:
            for item in self.database_map[data_set]:
                if(item.id == id):
                    item.name = updated_person.name
                    item.age = updated_person.age 
                    return item
        return None
    
    def delete_item(self, data_set,id:int) -> models.person.Person :
        with self._mutex:
            for index,item in enumerate(self.database_map[data_set]):
                if(item.id == id):
                    item = self.database_map[data_set].pop(index)
                    return item

    def get_all_data(self, dataset:str) -> list :
        if self.database_map.get(dataset):
            return self.database_map[dataset]
        return None

'''
A single object through which data is accessed.
So multiple database objects will not be created for 
multiple requests.
'''
dbContext = Database(utils.constants.DATABASE_FILE)