import http.server
import logging
import json
import models.person as person
import service.database_service as database_service
import re
import utils.helpers

class HTTPRequestHandler(http.server.BaseHTTPRequestHandler):

    def request_forbidden(self):
        '''
            Sends a 403 response to client when the client
            sends request to invalid url
        '''
        self.send_error(403)
        self.end_headers()


    def error_occured(self, msg:str):
        '''
            Send generic error to client incase of failure. 
            Doesn't log the error sepeatly in the server side.
        '''
        self.send_error(500,msg)
        self.end_headers()


    def resouce_not_found(self,):
        self.send_error(404)
        self.end_headers()


    def bad_request(self):
        self.send_error(400)
        self.end_headers()


    def send_Json_header(self, response_code):
        '''
            Send Content-Type : application/json
            header to client
        '''
        self.send_response(response_code)
        self.send_header('Content-Type','application/json')
        self.end_headers()


    def write_response_body(self, data:bytes):
        self.wfile.write(data)


    def send_complete_data(self,query):
        data = database_service.dbContext.get_all_data(query)
        if(data is None):
            self.resouce_not_found()
            return
        data_json = json.dumps({query : data}, default=utils.helpers.obj_dict)
        self.send_Json_header(200)
        self.write_response_body(data_json.encode('utf-8'))
        # self.end_headers() 
    

    def send_data_of_id(self,data_set,id):
        data = database_service.dbContext.find_item(data_set,id)
        
        if(data is None):
            self.resouce_not_found()
            return
        
        self.send_Json_header(200)
        self.write_response_body(json.dumps(data, default=utils.helpers.obj_dict).encode('utf-8'))
        self.end_headers()


    def get_person_data_from_request(self)->person.Person :
        '''
            When we a are expecting a person data from client
            run this function and get the person object
            
            Returns:
                ``data_object``: an object of Person class  
        '''
        data_length = int(self.headers.get('Content-Length'))
        logging.debug(f"Client sends data of length:{data_length}")
        if(data_length == 0):
            return None
        data = self.rfile.read(data_length).decode('utf-8')
        data_object = person.Person(**json.loads(data))
        return data_object


    def do_GET(self):
        logging.debug(f"GET Request recived: {self.path}")
        cl = int(self.headers.get('Content-Length'))
        logging.debug(f"Content Length: {self.headers.get('Content-Length')}")
        data = (self.rfile.read(cl)).decode('utf-8')
        logging.info(data)
        regex_match = re.match(r"^/(?P<query>person_data)(/|\?id=(?P<id>[0-9]+)/?)?$",self.path)

        logging.debug(f"{regex_match}")
        
        if(regex_match is None):
            self.request_forbidden()
            return
        data_set = regex_match['query']
        if(regex_match.groupdict().get('id') is None):
            #send whole data
            self.send_complete_data(data_set)
            return
        else:
            #Send the data corresponding to id
            id = int(regex_match['id'])
            self.send_data_of_id(data_set,id)
            return 
        
        
    def do_POST(self):
        regex_match = re.match(r"^/(?P<query>person_data)/?$",self.path)
        
        if(regex_match is None):
            self.request_forbidden()
            return 
        
        data_object = self.get_person_data_from_request()
        logging.debug(f"{data_object}")

        data_set = regex_match['query']
        if(data_object is None or data_object.containsNone()):
            self.bad_request()
            return
        
        data_object.id=None
        try:
            database_service.dbContext.add_entry(data_set,data_object)
            database_service.dbContext.save_to_database()
            self.send_response(201)
        except Exception as e: 
            self.error_occured("Error occured during addition")
            logging.error(e.__str__())
        self.end_headers()

    
    def do_PUT(self):

        regex_match = re.match(r"^/(?P<query>person_data)\?id=(?P<id>[0-9]+)/?$",self.path)
        
        logging.debug(f"{regex_match}")
        if(regex_match is None ):
            self.request_forbidden()
            return
        logging.debug(f"{regex_match.groups()}")
        
        id = int(regex_match['id'])
        data_set = regex_match['query']
        
        data_object = self.get_person_data_from_request()
        
        if data_object is None or data_object.containsNone():
            self.bad_request()
            return 
        
        updated_item = database_service.dbContext.update_item(data_set,id,data_object)
        
        if(updated_item is None):
            self.resouce_not_found()
            return
        
        database_service.dbContext.save_to_database()
        self.send_Json_header(201)
        self.write_response_body(json.dumps(updated_item, default=utils.helpers.obj_dict).encode('utf-8'))
        self.end_headers() 


    def do_DELETE(self):
        regex_match = re.match(r"^/(?P<query>person_data)\?id=(?P<id>[0-9]+)/?$",self.path)
        
        if(regex_match is None or regex_match.groupdict().get('id') is None):
            self.request_forbidden()
            self.send_error(403)
            return
        
        logging.debug(f"{regex_match.groups()}")

        id = int(regex_match['id'])
        data_set = regex_match['query']
        item = database_service.dbContext.delete_item(data_set,id)
        if(item is None):
            self.resouce_not_found()
            return
        database_service.dbContext.save_to_database()
        self.send_response(200)
        self.end_headers()