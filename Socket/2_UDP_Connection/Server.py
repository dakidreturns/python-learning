import socket
import logging
import sys
 
if __name__ == "__main__":
    HOST = "127.0.0.1"
    PORT = socket.htonl(34983)
    
    format = "[%(levelname)s] %(asctime)s : %(msg)s"
    time_format = "%H:%M:%S"

    logging.basicConfig(format=format, datefmt=time_format, level=logging.DEBUG)

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as server:
        server.settimeout(10)
        server.bind((HOST,PORT))
        logging.info(f"Socket bound to the adderess {HOST}:{PORT}")
        
        while True:
            try:
                data = server.recv(1024,)
            except TimeoutError as e:
                logging.debug("Connection timed out")
                sys.exit(1)
            logging.debug(f"Data from client: {data.hex(sep=" ")}")
            if not data:
                logging.info(f"Terminating connection from with client")
                break
            print(f"Data from client {data}")