import socket
import logging
import time

if __name__ == "__main__":
    HOST = "127.0.0.1"
    PORT = socket.htonl(34983)

    data = "This is a good sign"
    format = "[%(levelname)s] %(asctime)s : %(msg)s"
    time_format = "%H:%M:%S"

    logging.basicConfig(format=format, datefmt=time_format, level=logging.INFO)

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as client:
        print("Enter data to send to the server: ", end="")
        data = input()
        # time.sleep(2)
        logging.info(f"Sending data to server: {data}")
        client.sendto(bytes(data,"utf-8"), (HOST,PORT))   
        logging.info(f"Data Sent to server.")

        # server_data = client.recv(1024)
        # logging.info(f"Recived Data from server: {server_data!r}")
        # time.sleep(1)
        # logging.info("Terminating connection with server")
        