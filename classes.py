class A:
    def __init__(self, name):
        self.name = name
        print("Class A constructor")

class B(A):
    # def __init__(self, name):
    #     A.__init__(self,name )
    #     print("Class B constructor")

    def get_name(self):
        print(f"{self.name}")

if __name__=="__main__":
    b = B("python")
    b.get_name()