import sqlite3

con = sqlite3.connect("C:/Users/Rohith Karunakaran/Training/pythontest/Technical/Sources/Software/Python-Rohit/SQLite/example2.db")
cur = con.cursor()
# cur.execute("CREATE TABLE movie(title, year, score)")
# cur.execute("CREATE TABLE director(name, movie_id)")


# Getting all the table in the database
res = cur.execute("SELECT name FROM sqlite_master")
print(res.fetchall())

#Inserting values.
cur.execute('''
    INSERT INTO movie VALUES
            ('Monty Python and the holy grail', 1975, 8.2),
            ('And now for something completlyd different.', 1971, 7.5)
''')
con.commit()

# getting all the values in movies
res = cur.execute("SELECT score From movie")
res.fetchall()

# Multiple insertion.
movie_data = [
    ("Monty Python Live at the Hollywood Bowl", 1982, 7.9),
    ("Monty Python's The Meaning of Life", 1983, 7.5),
    ("Monty Python's Life of Brian", 1979, 8.0),
]

cur.executemany("insert into movie values(?,?,?)", movie_data )
con.commit()

res = cur.execute("SELECT * From movie")
print(res.fetchall())