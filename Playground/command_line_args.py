import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    # parser.add_argument("--conn", help="name of interface")
    
    subparser = parser.add_subparsers(title="--conn")
    parser_csv = subparser.add_parser(name="Serial")
    parser_csv.add_argument("csv")

    parser_json = subparser.add_parser(name="File")
    parser_json.add_argument("json")

    
    args = parser.parse_args(["--conn","Serial","csv"])
    print(args)